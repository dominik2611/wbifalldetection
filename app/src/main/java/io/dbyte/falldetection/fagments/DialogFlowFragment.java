package io.dbyte.falldetection.fagments;

import android.content.Context;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.androidplot.Plot;
import com.androidplot.util.Redrawer;
import com.androidplot.xy.*;
import io.dbyte.falldetection.R;
import io.dbyte.falldetection.recognition.SimpleRecognizer;
import io.dbyte.falldetection.sensors.SensorDataProvider;

import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DialogFlowFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DialogFlowFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DialogFlowFragment extends Fragment implements SensorDataProvider.SensorListener  {

    private String projectId = "testagent-dhutuf";
    private XYPlot plot;

    private XYPlot internalDataPlot;

    private SensorDataProvider dataProvider;

    private SimpleRecognizer simpleRecognizer;

    private final int HISTORY_SIZE = 200;

    private Redrawer redrawer;
    private Redrawer redrawer1;

    private SimpleXYSeries dataSeries[];

    private SimpleXYSeries internalDataSeries[];

    private  TextView txtvStatus;

    public DialogFlowFragment() {
        // Required empty public constructor
    }


    public static DialogFlowFragment newInstance() {
        DialogFlowFragment fragment = new DialogFlowFragment();
        Bundle args = new Bundle();


        fragment.setArguments(args);
        return fragment;
    }





    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_dialog_flow, container, false);

         txtvStatus = v.findViewById(R.id.txtvDetectionStatus);

        simpleRecognizer = new SimpleRecognizer(20);
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");

        simpleRecognizer.setFallDetectedListener(new SimpleRecognizer.FallDetectedListener() {
            @Override
            public void onFallDetected(String status) {

                String format = simpleDateFormat.format(new Date());
                txtvStatus.setText(format+ " "+status);
            }
        });

        dataProvider = new SensorDataProvider(getContext(), Sensor.TYPE_LINEAR_ACCELERATION);
        dataProvider.setReadingDataEnabled(false);
        dataProvider.setSensorListener(this);
        plot = v.findViewById(R.id.sensorPlot01);
        internalDataPlot = v.findViewById(R.id.sensorPlot02);

        configurePlot(plot);
        redrawer = new Redrawer(plot, 100, true);
        configurePlot(internalDataPlot);
        redrawer1 = new Redrawer(internalDataPlot, 100, true);
        dataSeries = new SimpleXYSeries[3];

        dataSeries[0] = new SimpleXYSeries("X");
        dataSeries[1] = new SimpleXYSeries("Y");
        dataSeries[2] = new SimpleXYSeries("Z");


        internalDataSeries = new SimpleXYSeries[3];

        internalDataSeries[0] = new SimpleXYSeries("Abs");
        internalDataSeries[1] = new SimpleXYSeries("Avg");
        internalDataSeries[2] = new SimpleXYSeries("Var");

        int colors[] = new int[dataSeries.length];
        colors[0] = Color.rgb(255, 0, 0);
        colors[1] = Color.rgb(0, 255, 0);
        colors[2] = Color.rgb(0, 0, 255);

        for (int i = 0; i < dataSeries.length; i++) {
            dataSeries[i].useImplicitXVals();
            plot.addSeries(dataSeries[i], new LineAndPointFormatter(colors[i], null, null, null));

        }

        for (int i = 0; i < internalDataSeries.length; i++) {
            internalDataSeries[i].useImplicitXVals();
            internalDataPlot.addSeries(internalDataSeries[i], new LineAndPointFormatter(colors[i], null, null, null));

        }


        dataProvider.setReadingDataEnabled(true);
        return v;
    }

    private void configurePlot(XYPlot plot) {

        plot.setRangeBoundaries(-10, 10, BoundaryMode.FIXED);
        plot.setDomainBoundaries(0, HISTORY_SIZE, BoundaryMode.FIXED);
        plot.setDomainStepMode(StepMode.INCREMENT_BY_VAL);
        plot.setDomainStepValue(HISTORY_SIZE / 10);
        plot.setLinesPerRangeLabel(3);
        plot.setRenderMode(Plot.RenderMode.USE_BACKGROUND_THREAD);

        plot.getGraph().getLineLabelStyle(XYGraphWidget.Edge.LEFT).
                setFormat(new DecimalFormat("#"));

        plot.getGraph().getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).
                setFormat(new DecimalFormat("#"));


    }


    public void onSensorDataArrived(SensorEvent event) {
        if (event.values.length == dataSeries.length) {


            for (int i = 0; i < dataSeries.length; i++) {

                if (dataSeries[i].size() > HISTORY_SIZE) {
                    dataSeries[i].removeFirst();


                }

                dataSeries[i].addLast(null, event.values[i]);


            }

            double absValues = simpleRecognizer.addSensorDataFrame(event.values);
            internalDataSeries[0].addLast(  null, absValues);
            double[] internalData = simpleRecognizer.getInternalData();
            internalDataSeries[1].addLast(null, internalData[0]);
            internalDataSeries[2].addLast(null, internalData[1]);
            for (int i = 0; i < internalDataSeries.length; i++) {
                if (internalDataSeries[i].size() > HISTORY_SIZE) {
                    internalDataSeries[i].removeFirst();


                }


            }
            plot.redraw();
            internalDataPlot.redraw();
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onSensorDataArrived(float[] data) {

    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
