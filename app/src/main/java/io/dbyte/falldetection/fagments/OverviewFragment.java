package io.dbyte.falldetection.fagments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Sensor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.speech.tts.Voice;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import androidx.fragment.app.Fragment;
import com.androidplot.util.Redrawer;
import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.XYPlot;
import io.dbyte.falldetection.R;
import io.dbyte.falldetection.data.PlotStorage;
import io.dbyte.falldetection.data.entities.PlotSet;
import io.dbyte.falldetection.data.entities.StoredPlot;
import io.dbyte.falldetection.recognition.DTWResult;
import io.dbyte.falldetection.recognition.DTWValue;
import io.dbyte.falldetection.recognition.MultiGestureRecognizer;
import io.dbyte.falldetection.sensors.DataProviderBase;
import io.dbyte.falldetection.sensors.DirectionAccProvider;
import io.dbyte.falldetection.sensors.SensorDataProvider;
import io.dbyte.falldetection.sensors.SensorPlot;
import io.dbyte.falldetection.voice.VoiceManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class OverviewFragment extends Fragment {

    private SensorPlot sensorPlot;
    private Redrawer redrawer;
    private final int HISTORY_SIZE = 200;

    private DataProviderBase dataProviderGravity;
    private DataProviderBase dataProviderLinAcc;
    private DataProviderBase dataProviderDirAcc;


    private SimpleXYSeries dataSeriesGravity[];
    private SimpleXYSeries dataSeriesLinAcc[];
    private SimpleXYSeries dataSeriesDirection[];


    private Button btnClearMarker;
    private Button btnSaveData;

    private TextView txtGestureDetectStatus;
    private TextView txtGestureDetectStatusDirAcc;
    private TextView txtGestureDetectStatusGrav;

    private TextView txtDetCountLinAcc;
    private TextView txtDetCountDir;
    private TextView txtDetCountGrav;

    private int detCountAccDir;
    private int detCountAccLin;
    private int detCountGrav;


    private HashMap<String, MultiGestureRecognizer> gestureRecognizers;


    private final OverviewFragment _ref;

    private boolean detectionActive = false;

    private TextView txtvStatus;


    private boolean clearRecognizerHistoryOnDetection = false;

    private boolean vibrateOnDetection = false;

    private Vibrator vibrator;

    public OverviewFragment() {
        _ref = this;
        // Required empty public constructor
    }


    public static OverviewFragment newInstance() {
        OverviewFragment fragment = new OverviewFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_overview, container, false);
        vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);

        txtvStatus = v.findViewById(R.id.txtvStatus);

        XYPlot plot = v.findViewById(R.id.sensorPlot);
        setupButtons(v);
        sensorPlot = new SensorPlot(plot, "Sensor", HISTORY_SIZE);
        sensorPlot.setPlotEventListener(new SensorPlot.PlotEventListener() {
            @Override
            public void onStoreData(int startX, int endX) {

            }

            @Override
            public void onDataStored(SimpleXYSeries storedSeries) {

            }

            @Override
            public void onSaveStateChanged(boolean canSave, SensorPlot sender) {
                btnClearMarker.setActivated(true);

            }

            @Override
            public void onPlotClicked() {
                dataProviderDirAcc.setReadingDataEnabled(false);
                dataProviderGravity.setReadingDataEnabled(false);
                dataProviderLinAcc.setReadingDataEnabled(false);
            }
        });

        dataProviderGravity = new SensorDataProvider(getContext(), Sensor.TYPE_GRAVITY);
        dataProviderLinAcc = new SensorDataProvider(getContext(), Sensor.TYPE_LINEAR_ACCELERATION);
        dataProviderDirAcc = new DirectionAccProvider(getContext());
        dataProviderDirAcc.start();


        dataSeriesGravity = setupDataSeries(3, new int[]{Color.RED, Color.GREEN, Color.BLUE}, plot);
        dataSeriesLinAcc = setupDataSeries(3, new int[]{Color.CYAN, Color.MAGENTA, Color.YELLOW}, plot);
        dataSeriesDirection = setupDataSeries(3, new int[]{Color.BLACK, Color.GRAY, Color.rgb(100, 100, 100)}, plot);

        dataProviderGravity.setSensorListener(new DataProviderBase.SensorListener() {
            @Override
            public void onSensorDataArrived(float[] data) {
                storeIntoDataSeries(data, dataSeriesGravity);
                if (detectionActive) {

                    MultiGestureRecognizer gestureRecognizer = gestureRecognizers.get("Gravity");
                    if (gestureRecognizer != null) {
                        gestureRecognizer.addToBuffer(new DTWValue(new float[]{data[0], data[1], data[2]}));
                        gestureRecognizer.detect();
                    }


                }
            }
        });

        dataProviderLinAcc.setSensorListener(new DataProviderBase.SensorListener() {
            @Override
            public void onSensorDataArrived(float[] data) {
                storeIntoDataSeries(data, dataSeriesLinAcc);
                if (detectionActive) {

                    MultiGestureRecognizer gestureRecognizer = gestureRecognizers.get("LinAcc");
                    if (gestureRecognizer != null) {
                        gestureRecognizer.addToBuffer(new DTWValue(new float[]{data[0], data[1], data[2]}));
                        gestureRecognizer.detect();
                    }


                }
            }
        });

        dataProviderDirAcc.setSensorListener(new DataProviderBase.SensorListener() {
            @Override
            public void onSensorDataArrived(float[] data) {

                storeIntoDataSeries(data, dataSeriesDirection);
                if (detectionActive) {

                    MultiGestureRecognizer gestureRecognizer = gestureRecognizers.get("DirAcc");
                    if (gestureRecognizer != null) {
                        gestureRecognizer.addToBuffer(new DTWValue(new float[]{data[0], data[1], data[2]}));
                        gestureRecognizer.detect();
                    }


                }
            }
        });

        v.findViewById(R.id.btnStartVoice).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setupVoice();
            }
        });
        return v;
    }

    private void setupVoice(){


        requestRecordAudioPermission();
        requestPhonePermission();

        VoiceManager vm = new VoiceManager(getContext(), this);
        vm.start();
    }

    private void requestRecordAudioPermission() {
        String requiredPermission = Manifest.permission.RECORD_AUDIO;

        // If the user previously denied this permission then show a message explaining why
        // this permission is needed
        if (getActivity().checkCallingOrSelfPermission(requiredPermission) == PackageManager.PERMISSION_DENIED) {
            requestPermissions(new String[]{requiredPermission}, 101);
        }
    }

    private void requestPhonePermission() {
        String requiredPermission = Manifest.permission.CALL_PHONE;

        // If the user previously denied this permission then show a message explaining why
        // this permission is needed
        if (getActivity().checkCallingOrSelfPermission(requiredPermission) == PackageManager.PERMISSION_DENIED) {
            requestPermissions(new String[]{requiredPermission}, 101);
        }
    }


    private void setupGestureRecognizer(PlotSet ps) {
        Log.d("setup", "recgonizer for " + ps.setName);

        List<StoredPlot> storedPlots = ps.storedPlots;
        DTWValue[] dtwValues = new DTWValue[storedPlots.get(0).yValues.size()];
        for (int i = 0; i < dtwValues.length; i++) {
            float[] v = new float[storedPlots.size()];
            for (int j = 0; j < storedPlots.size(); j++) {
                v[j] = storedPlots.get(j).yValues.get(i);

            }
            DTWValue dtwValue = new DTWValue(v);
            dtwValues[i] = dtwValue;
        }


        MultiGestureRecognizer gestureRecognizer = new MultiGestureRecognizer(42, "", dtwValues,
                dtwValues.length / 2, 5);
        gestureRecognizer.setGestureRecognizedListener(new MultiGestureRecognizer.GestureRecognizedListener() {
            @Override
            public void onGestureRecognized(final MultiGestureRecognizer sender, final DTWResult result) {
                // txtvMultiResult.setText(sender.getName() + " " + result.getDistance());
             //   Log.d("recognized", "gesture ++++++++++++++++++++++++++++");
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        txtGestureDetectStatus.setText("Detected " + result.getDistance() + " on " + sender.getName());
                    }
                });
            }
        });

        String psName = ps.setName;
        if (psName.contains("Gravity")) {
            gestureRecognizer.setName("Gravity");
            gestureRecognizer.setGestureRecognizedListener(new MultiGestureRecognizer.GestureRecognizedListener() {
                @Override
                public void onGestureRecognized(final MultiGestureRecognizer sender, final DTWResult result) {
                    // txtvMultiResult.setText(sender.getName() + " " + result.getDistance());
                 //   Log.d("recognized", "gesture ++++++++++++++++++++++++++++");
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            txtGestureDetectStatusGrav.setText("Detected " + result.getDistance() + " on " + sender.getName());
                            detCountGrav++;
                            txtDetCountDir.setText("Grav count: "+detCountGrav);
                            if(clearRecognizerHistoryOnDetection){
                                sender.clearBuffer();
                            }
                            if(vibrateOnDetection){
                                vibrate();
                            }
                        }
                    });
                }
            });


        } else if (psName.contains("DirAcc")) {
            gestureRecognizer.setName("DirAcc");
            gestureRecognizer.setGestureRecognizedListener(new MultiGestureRecognizer.GestureRecognizedListener() {
                @Override
                public void onGestureRecognized(final MultiGestureRecognizer sender, final DTWResult result) {
                    // txtvMultiResult.setText(sender.getName() + " " + result.getDistance());
                   // Log.d("recognized", "gesture ++++++++++++++++++++++++++++");
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            txtGestureDetectStatusDirAcc.setText("Detected " + result.getDistance() + " on " + sender.getName());
                            detCountAccDir++;
                            txtDetCountDir.setText("Acc Dir count: "+detCountAccDir);
                            if(clearRecognizerHistoryOnDetection){
                                sender.clearBuffer();
                            }
                            if(vibrateOnDetection){
                                vibrate();
                            }
                        }
                    });
                }
            });

        } else if (psName.contains("LinAcc")) {
            gestureRecognizer.setName("LinAcc");
            gestureRecognizer.setGestureRecognizedListener(new MultiGestureRecognizer.GestureRecognizedListener() {
                @Override
                public void onGestureRecognized(final MultiGestureRecognizer sender, final DTWResult result) {
                    // txtvMultiResult.setText(sender.getName() + " " + result.getDistance());
                  //  Log.d("recognized", "gesture ++++++++++++++++++++++++++++");
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            txtGestureDetectStatus.setText("Detected " + result.getDistance() + " on " + sender.getName());
                            detCountAccLin++;
                            txtDetCountDir.setText("Lin Acc count: "+detCountAccLin);
                            if(clearRecognizerHistoryOnDetection){
                                sender.clearBuffer();
                            }
                            if(vibrateOnDetection){
                                vibrate();
                            }
                        }
                    });
                }
            });

        }
        gestureRecognizers.put(gestureRecognizer.getName(), gestureRecognizer);

    }


    private void setupButtons(final View v) {

        CheckBox chkbxGravity = v.findViewById(R.id.chkbxGravity);
        CheckBox chkbxLinAcc = v.findViewById(R.id.chkbxLinAcc);
        CheckBox chkbxDirAcc = v.findViewById(R.id.chkbxDirAcc);
        Button btnReset = v.findViewById(R.id.btnReset);
        Button btnStop = v.findViewById(R.id.btnStop);
        Button btnChangeThreshold = v.findViewById(R.id.btnChangeThreshold);
        CheckBox chkbxDetect = v.findViewById(R.id.chkbxDetect);

        btnClearMarker = v.findViewById(R.id.btnClearMarker);
        btnSaveData = v.findViewById(R.id.btnSaveData);

        txtGestureDetectStatus = v.findViewById(R.id.txtvDetectStatus);
        txtGestureDetectStatusDirAcc = v.findViewById(R.id.txtvDetectDirAcc);
        txtGestureDetectStatusGrav = v.findViewById(R.id.txtvDetectGrav);

        txtDetCountDir = v.findViewById(R.id.txtvDetCountDir);
        txtDetCountGrav = v.findViewById(R.id.txtvDetCountGrav);
        txtDetCountLinAcc = v.findViewById(R.id.txtvDetCountLinAcc);



        final EditText etxtThreshold = v.findViewById(R.id.etxtThreshold);



        btnChangeThreshold.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String content = etxtThreshold.getText().toString();
                if (content != null && !content.isEmpty()) {
                    for (MultiGestureRecognizer gestureRecognizer : gestureRecognizers.values()) {
                        gestureRecognizer.setRecognizedThreshold(Double.parseDouble(content));
                    }

                }

            }
        });

        btnClearMarker.setActivated(false);
        btnSaveData.setActivated(false);

        btnClearMarker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sensorPlot.clearMarker();
            }
        });

        btnSaveData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int[] savePoints = sensorPlot.getSavePoints();
                storeDataSeries(savePoints[0], savePoints[1], dataSeriesGravity, "Gravity");
                storeDataSeries(savePoints[0], savePoints[1], dataSeriesLinAcc, "LinAcc");
                storeDataSeries(savePoints[0], savePoints[1], dataSeriesDirection, "DirAcc");


                int size = dataSeriesLinAcc[0].size();
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < size; i++) {
                    sb.append(dataSeriesLinAcc[0].getyVals().get(i) + "|" + dataSeriesLinAcc[1].getyVals().get(i) + "|" + dataSeriesLinAcc[2].getyVals().get(i));
                }

                Log.d("saved ", "data: " + sb.toString());

            }
        });


        ((CheckBox)v.findViewById(R.id.chkbxClearHistory)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                clearRecognizerHistoryOnDetection = isChecked;
            }
        });

        ((CheckBox)v.findViewById(R.id.chkbxVibrate)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                vibrateOnDetection = isChecked;
            }
        });

        v.findViewById(R.id.btnClearCount).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                detCountAccDir  = 0;
                detCountAccLin = 0;
                detCountGrav = 0;

                txtDetCountDir.setText("");
                txtDetCountGrav.setText("");
                txtDetCountLinAcc.setText("");
            }
        });

        chkbxDetect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                gestureRecognizers = new HashMap<>();
                if (isChecked) {
                    AsyncTask.execute(new Runnable() {
                        @Override
                        public void run() {
                            // Insert Data

                            final List<PlotSet> plots = PlotStorage.getInstance(getContext()).getPlotSets();

                            for (PlotSet plot : plots) {
                                if (plot.inUse) {

                                    String plotData = "";
                                    for (StoredPlot storedPlot : plot.storedPlots) {
                                        plotData += " plot: ";
                                        for (Float value : storedPlot.yValues) {
                                            plotData += " | " + value + " | ";
                                        }

                                    }

                                    Log.d("plot data", "for " + plot.setName + " " + plot.saveDate.toString() + " " + plotData);


                                    _ref.setupGestureRecognizer(plot);
                                    detectionActive = true;
                                }
                            }

                        }
                    });
                } else {
                    detectionActive = false;
                }
            }
        });


        chkbxGravity.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                dataProviderGravity.setReadingDataEnabled(isChecked);
            }
        });

        chkbxLinAcc.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                dataProviderLinAcc.setReadingDataEnabled(isChecked);
            }
        });


        chkbxDirAcc.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                dataProviderDirAcc.setReadingDataEnabled(isChecked);
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearSeries(dataSeriesDirection);
                clearSeries(dataSeriesGravity);
                clearSeries(dataSeriesLinAcc);
            }
        });

        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataProviderDirAcc.setReadingDataEnabled(false);
                dataProviderGravity.setReadingDataEnabled(false);
                dataProviderLinAcc.setReadingDataEnabled(false);
            }
        });


    }


    private void clearSeries(SimpleXYSeries[] series) {
        for (int i = 0; i < series.length; i++) {
            series[i].clear();

        }
    }

    private void storeDataSeries(final int startX, final int endX, final SimpleXYSeries[] dataSeries, final String name) {


        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                String status = "saving: \n";
                PlotSet ps = new PlotSet(0, name, new Date());
                List<StoredPlot> storedPlots = new ArrayList<>();
                for (int i = 0; i < dataSeries.length; i++) {
                    if (dataSeries[i].size() > endX -startX) {
                        StoredPlot series = storeSeries(startX, endX, dataSeries[i]);
                        storedPlots.add(series);
                        status += "\n" + "store: " + dataSeries[i].getTitle() + " " + series.yValues.size();
                    }


                }

                final String finalStatus = status;
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        txtvStatus.setText(txtvStatus.getText() + finalStatus);
                    }
                });

                Log.d("saved ", finalStatus);


                ps.storedPlots = storedPlots;


                PlotStorage.getInstance(getContext()).storePlot(ps);
            }
        });
    }

    private StoredPlot storeSeries(int startX, int endX, SimpleXYSeries series) {
        ArrayList<Float> yVals = new ArrayList<>();
Log.d("save series", "start: "+startX + " end: "+endX + " length: "+series.getyVals().size());
        for (int i = startX; i < endX; i++) {
            Number x = series.getX(i);
            Number y = series.getY(i);
            yVals.add(y.floatValue());
        }
        StoredPlot sp = new StoredPlot(series.getTitle(), yVals);
        return sp;

    }

    private SimpleXYSeries[] setupDataSeries(int dim, int[] colors, XYPlot xyPlot) {
        SimpleXYSeries[] dataSeries = new SimpleXYSeries[3];

        dataSeries[0] = new SimpleXYSeries("X");
        dataSeries[1] = new SimpleXYSeries("Y");
        dataSeries[2] = new SimpleXYSeries("Z");


        for (int i = 0; i < dataSeries.length; i++) {
            dataSeries[i].useImplicitXVals();
            dataSeries[i].useImplicitXVals();
            xyPlot.addSeries(dataSeries[i], new LineAndPointFormatter(colors[i], null, null, null));


        }
        return dataSeries;

    }

    private void storeIntoDataSeries(float[] data, SimpleXYSeries[] dataSeries) {
        if (data.length >= dataSeries.length) {


            for (int i = 0; i < dataSeries.length; i++) {

                if (dataSeries[i].size() > HISTORY_SIZE) {
                    dataSeries[i].removeFirst();

                }

                dataSeries[i].addLast(null, data[i]);
//                if (isDetecting) {
//                    continousGestureRecognizers[i].addToBuffer(data[i]);
//                    continousGestureRecognizers[i].detect();
//
//
//                }

            }

        }
        sensorPlot.getPlot().redraw();
    }


    private void vibrate(){

        VibrationEffect effect = VibrationEffect.createOneShot(350, VibrationEffect.DEFAULT_AMPLITUDE);
        vibrator.vibrate(effect);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

}
