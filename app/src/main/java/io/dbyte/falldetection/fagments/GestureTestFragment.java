package io.dbyte.falldetection.fagments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.net.Uri;
import android.opengl.Matrix;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.*;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidplot.Plot;
import com.androidplot.util.Redrawer;
import com.androidplot.xy.*;
import io.dbyte.falldetection.R;
import io.dbyte.falldetection.recognition.*;
import io.dbyte.falldetection.sensors.SensorDataProvider;
import org.w3c.dom.Text;

import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.concurrent.Semaphore;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link GestureTestFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link GestureTestFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GestureTestFragment extends Fragment implements SensorDataProvider.SensorListener {


    private OnFragmentInteractionListener mListener;

    private SensorDataProvider dataProvider;
    private SensorDataProvider dataProviderGravity;
    private final int HISTORY_SIZE = 200;

    private Redrawer redrawer;

    private SimpleXYSeries dataSeries[];

    private SimpleXYSeries dataSeriesDirection[];

    private XYPlot plot;

    private double[][] referenceData;
    DTWValue[] combinedRefData;


    private boolean isRecording = false;

    private boolean isDetecting = false;

    private TextView txtvStatus;

    private TextView txtvMultiResult;

    private GestureRecognizer[] gestureRecognizers;


    private GestureRecognizer[] continousGestureRecognizers;

    private MultiGestureRecognizer multiGestureRecognizer;


    private TextView[] txtvResultSingleRecs;

    public GestureTestFragment() {
        // Required empty public constructor

    }


    public static GestureTestFragment newInstance() {
        GestureTestFragment fragment = new GestureTestFragment();


        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_gesture_test, container, false);

        dataProvider = new SensorDataProvider(getContext(), Sensor.TYPE_LINEAR_ACCELERATION);
        dataProvider.setReadingDataEnabled(false);
        dataProvider.setSensorListener(this);

        dataProviderGravity = new SensorDataProvider(getContext(), Sensor.TYPE_GRAVITY);



        gestureRecognizers = new GestureRecognizer[3];
        continousGestureRecognizers = new GestureRecognizer[3];

        txtvResultSingleRecs = new TextView[3];
        txtvResultSingleRecs[0] = view.findViewById(R.id.txtvResultSingle01);
        txtvResultSingleRecs[1] = view.findViewById(R.id.txtvResultSingle02);
        txtvResultSingleRecs[2] = view.findViewById(R.id.txtvResultSingle03);

        txtvStatus = view.findViewById(R.id.txtvStatus);


        CheckBox chkbxMode = view.findViewById(R.id.chkbxMode);
        chkbxMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isDetecting = isChecked;
                dataProvider.setReadingDataEnabled(true);
            }
        });

        Button btnStartRecord = view.findViewById(R.id.btnStartRecord);
        btnStartRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startRecording();
            }
        });

        Button btnSave = view.findViewById(R.id.btnSaveData);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataProvider.setReadingDataEnabled(false);
                txtvStatus.setText("Saved reference Data!");
                referenceData = storeData();
                combinedRefData = storeMultiData();
                setupContinouus();
                resetData();
            }


        });


        Button btnTrack = view.findViewById(R.id.btnStartTracking);
        btnTrack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                detect();


            }
        });

        txtvMultiResult = view.findViewById(R.id.txtvCombined);

        final EditText txtThreshold = view.findViewById(R.id.etxtThreshold);


        Button btnThreshold = view.findViewById(R.id.btnSetThreshold);
        btnThreshold.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < continousGestureRecognizers.length; i++) {
                    GestureRecognizer continousGestureRecognizer = continousGestureRecognizers[i];
                    String content = txtThreshold.getText().toString();
                    if (continousGestureRecognizer != null && content != null && !content.isEmpty()) {
                        continousGestureRecognizer.setRecognizedThreshold(Double.parseDouble(content));
                    }

                }
            }
        });


        final EditText txtThresHoldCombined = view.findViewById(R.id.etxtCombinedThres);

        Button btnCombinedThres = view.findViewById(R.id.btnCombinedThresOk);
        btnCombinedThres.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (multiGestureRecognizer != null) {
                    String content = txtThresHoldCombined.getText().toString();
                    if (content != null && !content.isEmpty()) {
                        multiGestureRecognizer.setRecognizedThreshold(Double.parseDouble(content));
                    }
                }
            }
        });


        plot = view.findViewById(R.id.sensorPlot);

        plot.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d("touch ", "event: " + event.getAction());
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    Log.d("sa", "down");
                    // Start recording
                    startRecording();
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    Log.d("sa", "upassad");
                    // stop recording
                    if (!isDetecting) {
                        txtvStatus.setText("Saved reference Data!");
                        referenceData = storeData();
                        resetData();
                    } else {
                        detect();
                    }
                }
                return true;
            }
        });
        configurePlot(plot);
        dataSeries = new SimpleXYSeries[3];

        dataSeries[0] = new SimpleXYSeries("X");
        dataSeries[1] = new SimpleXYSeries("Y");
        dataSeries[2] = new SimpleXYSeries("Z");


        dataSeriesDirection = new SimpleXYSeries[3];
        dataSeriesDirection[0] = new SimpleXYSeries("X-Dir");
        dataSeriesDirection[1] = new SimpleXYSeries("Y-Dir");
        dataSeriesDirection[2] = new SimpleXYSeries("Z-Dir");

        int colors[] = new int[dataSeries.length];
        colors[0] = Color.rgb(255, 0, 0);
        colors[1] = Color.rgb(0, 255, 0);
        colors[2] = Color.rgb(0, 0, 255);

        for (int i = 0; i < dataSeries.length; i++) {
            dataSeries[i].useImplicitXVals();
            dataSeriesDirection[i].useImplicitXVals();
            plot.addSeries(dataSeries[i], new LineAndPointFormatter(colors[i], null, null, null));


        }

        int colors2[] = new int[dataSeries.length];
        colors2[0] = Color.rgb(255, 0, 255);
        colors2[1] = Color.rgb(255, 255, 0);
        colors2[2] = Color.rgb(0, 255, 255);

        for (int i = 0; i < dataSeriesDirection.length; i++) {

            dataSeriesDirection[i].useImplicitXVals();
            plot.addSeries(dataSeriesDirection[i], new LineAndPointFormatter(colors2[i], null, null, null));


        }

        final TextView txtvAngle = view.findViewById(R.id.txtvAngles);
        final TextView txtvAngle2 = view.findViewById(R.id.txtvAngle2);
        final TextView txtvAngle3 = view.findViewById(R.id.txtvAngle3);
        final TextView txtTheta = view.findViewById(R.id.txtvTheta);
        TextView txtPhi = view.findViewById(R.id.txtvPhi);

        dataProviderGravity.setSensorListener(new SensorDataProvider.SensorListener() {
            @Override
            public void onSensorDataArrived(float[] data) {
//                double[] angles = calculate(data[0], data[1]);
//                double[] angles3 = calculate3(data[0], event.values[1], event.values[2]);
//                txtvAngle.setText("angle: x   "+angles[0]);
//                txtvAngle2.setText("angle: y  "+angles[1]);
//                txtvAngle3.setText("angle: 3"+angles3[2]);
//
//                double[] coords = calcCoords(event.values);
//
//                txtTheta.setText("T: "+coords[0]);
//                txtPhi.setText("P: "+coords[1]);
//
//                storeIntoDataSeries(event, dataSeriesDirection);
            }


        });

        dataProviderGravity.setReadingDataEnabled(true);
        return view;
    }





    public  double[] calculate(double force1, double force2){
        double resulting = 9.81;
        double res = Math.sqrt((force1 * force1) + (force2 * force2));
       // System.out.println(res);

        // calculate angle
        double angle1 = Math.atan(force2 / force1);
       //ystem.out.println("angle 1: "+angle1*(180/Math.PI));

        double angle2 = Math.atan(force1 / force2);
        //System.out.println("angle 2: "+);
        return new double[] {radToDeg(angle1), radToDeg(angle2)};
    }

    public double[] calculate3(double force1, double force2, double force3){
        double resulting = 9.81;
        double res = Math.sqrt((force1 * force1) + (force2 * force2)+(force3*force3));
       // System.out.println(res);

        // calculate angle
        double angle1 = Math.atan(force2/ force1);
       // System.out.println("angle 1: "+angle1*(180/Math.PI));

        double angle2 = Math.atan(force1 / force2);
        //System.out.println("angle 2: "+angle2*(180/Math.PI));

        double angle3 = Math.atan(force3 / res);
       System.out.println("angle 1: "+angle1*(180/Math.PI)+ " angle 2: "+angle2*(180/Math.PI)+"angle 3: "+angle3*(180/Math.PI));
        return new double[] {radToDeg(angle1), radToDeg(angle2), radToDeg(angle3)};
    }

    private double[] calcCoords(float[] v){
        double r = Math.sqrt(v[0]*v[0]+v[1]*v[1]+v[2]*v[2]);
        double the = Math.acos(v[2]/r);

        double phi = Math.atan2(v[1], v[0]);

        return new double[]{radToDeg(the), radToDeg(phi)};


    }

    private double radToDeg(double rad){
       return rad*(180/Math.PI);
    }


    public static void rotateCoordinates(double angle, double x, double y){
        double x1 = x * Math.cos(angle) +y * Math.cos(angle);
        double y1 = -1*x*Math.sin(angle) + y*Math.cos(angle);

    }

    private void setupContinouus() {

        for (int i = 0; i < referenceData.length; i++) {
            continousGestureRecognizers[i] = new GestureRecognizer(i, "con-" + i, referenceData[i],
                    referenceData[i].length / 2,
                    0.1, 500, referenceData[i].length * 3, referenceData[i].length);
            continousGestureRecognizers[i].setGestureRecognizedListener(new GestureRecognizer.GestureRecognizedListener() {
                @Override
                public void onGestureRecognized(GestureRecognizer sender, DTWResult result) {
                    //   Log.d("recognizer", "gesture recognized from " + sender.getName());
                    txtvResultSingleRecs[sender.getId()].setText(sender.getName() + " result: " + result.getDistance());
                    //clearRecognizersBuffer();
                }
            });

        }


        multiGestureRecognizer = new MultiGestureRecognizer(42, "multi", combinedRefData,
                combinedRefData.length / 2, 0.1);

        multiGestureRecognizer.setGestureRecognizedListener(new MultiGestureRecognizer.GestureRecognizedListener() {
            @Override
            public void onGestureRecognized(MultiGestureRecognizer sender, DTWResult result) {
                txtvMultiResult.setText(sender.getName() + " " + result.getDistance());
            }
        });
    }

    private void clearRecognizersBuffer() {
        for (int i = 0; i < continousGestureRecognizers.length; i++) {
            GestureRecognizer continousGestureRecognizer = continousGestureRecognizers[i];
            if (continousGestureRecognizer != null) {
                continousGestureRecognizer.clearBuffer();
            }

        }
    }

    private void startRecording() {
        resetData();
        dataProvider.setReadingDataEnabled(true);
        txtvStatus.setText("Recording started");
    }

    private void detect() {
        dataProvider.setReadingDataEnabled(false);
        txtvStatus.setText("Tracking: ");
//        for (int i = 0; i < referenceData.length; i++) {
//            gestureRecognizers[i] = new GestureRecognizer(i,"rec-" + i, referenceData[i], referenceData[i].length / 2,
//                    3, 500, referenceData[i].length * 3, referenceData[i].length);
//            gestureRecognizers[i].setGestureRecognizedListener(new GestureRecognizer.GestureRecognizedListener() {
//                @Override
//                public void onGestureRecognized(String recName) {
//                    Log.d("recognizer", "gesture recognized from " + recName);
//
//                   // txtvResult.setText(txtvResult.getText() + " found: " + recName + "! ");
//                }
//            });
//
//        }
        isDetecting = true;
        double[][] testData = storeData();
        for (int i = 0; i < testData.length; i++) {
            for (int j = 0; j < testData[i].length; j++) {
                gestureRecognizers[i].addToBuffer(testData[i][j]);
            }
            // TODO async
            gestureRecognizers[i].detect();
        }


    }


    private double[][] storeData() {
        double[][] data = new double[dataSeries.length][];

        // stop recording


        // get data

        for (int i = 0; i < data.length; i++) {
            double[] yVals = new double[dataSeries[i].size()];
            LinkedList<Number> yValList = dataSeries[i].getyVals();
            for (int j = 0; j < yValList.size(); j++) {
                yVals[j] = yValList.get(j).floatValue();
            }
            data[i] = yVals;
        }


        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < data.length; i++) {
            sb.append("Data " + i + ": ");
            for (int j = 0; j < data[i].length; j++) {
                sb.append(data[i][j] + " | ");

            }
            sb.append("\n");
        }
        Log.d("stored data", sb.toString());
        return data;
    }

    private DTWValue[] storeMultiData() {
        int valueCount = dataSeries[0].size();
        DTWValue[] values = new DTWValue[valueCount];

        for (int i = 0; i < valueCount; i++) {
            float[] f = new float[dataSeries.length];
            for (int j = 0; j < f.length; j++) {
                f[j] = dataSeries[j].getyVals().get(i).floatValue();
            }
            DTWValue dv = new DTWValue(f);
            values[i] = dv;
        }


        return values;
    }

    private void resetData() {
        for (SimpleXYSeries ds : dataSeries) {
            ds.clear();
        }
        clearRecognizersBuffer();
    }


    private void configurePlot(XYPlot plot) {

        plot.setRangeBoundaries(-10, 10, BoundaryMode.FIXED);
        plot.setDomainBoundaries(0, HISTORY_SIZE, BoundaryMode.FIXED);
        plot.setDomainStepMode(StepMode.INCREMENT_BY_VAL);
        plot.setDomainStepValue(HISTORY_SIZE / 10);
        plot.setLinesPerRangeLabel(3);
        plot.setRenderMode(Plot.RenderMode.USE_BACKGROUND_THREAD);

        plot.getGraph().getLineLabelStyle(XYGraphWidget.Edge.LEFT).
                setFormat(new DecimalFormat("#"));

        plot.getGraph().getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).
                setFormat(new DecimalFormat("#"));

        redrawer = new Redrawer(plot, 100, true);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }



    private void storeIntoDataSeries(float[] data, SimpleXYSeries[] dataSeries){
        if (data.length == dataSeries.length) {


            for (int i = 0; i < dataSeries.length; i++) {

                if (dataSeries[i].size() > HISTORY_SIZE) {
                    dataSeries[i].removeFirst();

                }

                dataSeries[i].addLast(null, data[i]);
                if (isDetecting) {
                    continousGestureRecognizers[i].addToBuffer(data[i]);
                    continousGestureRecognizers[i].detect();


                }

            }

        }
        }

    @Override
    public void onSensorDataArrived(float[] data) {
        storeIntoDataSeries(data, dataSeries);

        if (isDetecting) {
            multiGestureRecognizer.addToBuffer(new DTWValue(data));
            multiGestureRecognizer.detect();
        }

        plot.redraw();

    }


    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
