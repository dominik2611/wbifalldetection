package io.dbyte.falldetection.fagments;

import android.content.Context;
import android.hardware.Sensor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.androidplot.xy.*;
import io.dbyte.falldetection.R;
import io.dbyte.falldetection.data.PlotStorage;
import io.dbyte.falldetection.data.entities.PlotSet;
import io.dbyte.falldetection.data.entities.StoredPlot;
import io.dbyte.falldetection.sensors.SensorDataProvider;
import io.dbyte.falldetection.sensors.SensorPlot;
import io.dbyte.falldetection.sensors.SensorPlotGroup;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AccelerationSensor.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AccelerationSensor#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AccelerationSensor extends Fragment implements SensorPlot.PlotEventListener {

    private static final int HISTORY_SIZE = 1000;


    private SimpleXYSeries[] sensorSeries;


    private OnFragmentInteractionListener mListener;


   // private SensorDataProvider sensorDataProvider;

    private SensorPlotGroup accSensors;

    private boolean canSave = false;

    private Button btnSave;

    public AccelerationSensor() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AccelerationSensor.
     */
    // TODO: Rename and change types and number of parameters
    public static AccelerationSensor newInstance(String param1, String param2) {
        AccelerationSensor fragment = new AccelerationSensor();


        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //sensorDataProvider = new SensorDataProvider(getContext(), Sensor.TYPE_LINEAR_ACCELERATION);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_acceleration_sensor, container, false);

//        final Button btnStartStop = view.findViewById(R.id.btnStop);
//        btnStartStop.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d("control", "toggle recording");
//                setRecordingState(!sensorDataProvider.isReadingData(), btnStartStop);
//
//            }
//        });
//        setRecordingState(true, btnStartStop);
//
//
//        final Button btnPanZoom = view.findViewById(R.id.btnZoom);
//        btnPanZoom.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d("control", "toggle zoom");
//                setZoomingState(!accSensors.canPanZoom(), btnPanZoom);
//
//            }
//        });
//
//        final Button btnReset = view.findViewById(R.id.btnReset);
//        btnReset.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d("control", "toggle zoom");
//                accSensors.clearAll();
//
//                AsyncTask.execute(new Runnable() {
//                    @Override
//                    public void run() {
//                        // Insert Data
//
//                        List<PlotSet> plots = PlotStorage.getInstance(getContext()).getPlotSets();
//                        for (PlotSet plotSet : plots) {
//                            Log.d("plots", "plot: " + plotSet.setName + " " + plotSet.saveDate.toString() + " " + plotSet.setId);
//
//                            for (StoredPlot storedPlot : plotSet.storedPlots) {
//                                StringBuilder sb = new StringBuilder();
//                                Log.d("values for ", "plot " + storedPlot.plotName);
//                                for (Float yValue : storedPlot.yValues) {
//                                    sb.append("val: " + yValue);
//                                }
//                                Log.d("vals: ", "---" + sb.toString());
//                            }
//                        }
//
//                    }
//                });
//
//
//            }
//        });
//
//        btnSave = view.findViewById(R.id.btnSave);
//        btnSave.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d("control", "save");
//                accSensors.saveData();
//
//                AsyncTask.execute(new Runnable() {
//                    @Override
//                    public void run() {
//                        // Insert Data
//                        openSaveDialog();
////                        PlotStorage.getInstance(getContext()).storePlot();
//
//
//                    }
//                });
//
//
//            }
//        });
//
//        btnSave.setEnabled(canSave);
//
//        int historySize = 1000;
//        SensorPlot[] accSensorPlots = new SensorPlot[3];
//
//
//        accSensors = new SensorPlotGroup(3);
//        accSensors.setSensorListener(this);
//        accSensors.setSensorDataProvider(sensorDataProvider);
//        accSensors.addPlot(new SensorPlot((XYPlot) view.findViewById(R.id.plotAccX), "X", historySize));
//        accSensors.addPlot(new SensorPlot((XYPlot) view.findViewById(R.id.plotAccY), "Y", historySize));
//        accSensors.addPlot(new SensorPlot((XYPlot) view.findViewById(R.id.plotAccZ), "Z", historySize));
//
//
//        setZoomingState(false, btnPanZoom);

        return view;
    }

    private void openSaveDialog() {
        SaveDialogFragment newFragment = new SaveDialogFragment();
        newFragment.setDialogListener(new SaveDialogFragment.SaveDialogListener() {
            @Override
            public void onDialogOk(final String name) {
                Log.d("strogin plot ", "with name " + name);

                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        // Insert Data

                        PlotSet ps = new PlotSet(0, name, new Date());
                        ps.storedPlots = new ArrayList<>();
                        List<SensorPlot> sensorPlots = accSensors.getSensorPlots();
                        for (SensorPlot sensorPlot : sensorPlots) {
                            StoredPlot storedPlot = sensorPlot.prepareSave();
                            ps.storedPlots.add(storedPlot);
                        }

                        PlotStorage.getInstance(getContext()).storePlot(ps);
                    }
                });


            }

            @Override
            public void onDialogCancel() {

            }
        });
        newFragment.show(getActivity().getSupportFragmentManager(), "missiles");


    }

    private void setRecordingState(boolean isRecording, Button btnRecord) {
        //sensorDataProvider.setReadingDataEnabled(isRecording);
        if (isRecording) {
            btnRecord.setText("Stop");
        } else {
            btnRecord.setText("Start");
        }
    }

    private void setZoomingState(boolean canPanZoom, Button btnZoom) {
        accSensors.setPanZoomEnabled(canPanZoom);
        if (accSensors.canPanZoom()) {
            btnZoom.setText("Disable PanZoom");
        } else {
            btnZoom.setText("Enable PanZoom");
        }


    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onStoreData(int startX, int endX) {

    }

    @Override
    public void onDataStored(SimpleXYSeries storedSeries) {

    }

    @Override
    public void onSaveStateChanged(boolean canSave, SensorPlot sender) {
        Log.d("saving", "save");
        this.canSave = canSave;
        if (btnSave != null) {
            btnSave.setEnabled(canSave);
        }
    }

    @Override
    public void onPlotClicked() {

    }


    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }


}
