package io.dbyte.falldetection.fagments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import io.dbyte.falldetection.R;


public class SaveDialogFragment extends DialogFragment {

    public void setDialogListener(SaveDialogListener dialogListener) {
        this.dialogListener = dialogListener;
    }

    public interface SaveDialogListener {
        void onDialogOk(String name);

        void onDialogCancel();
    }


    private SaveDialogListener dialogListener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_save_dialog, null);

        final EditText txtPlotName = view.findViewById(R.id.save_plot_name);
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(view).setMessage("Save Plot:")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        Log.d("dialog", "OK: " + txtPlotName.getText());
                        if (dialogListener != null) {
                            dialogListener.onDialogOk(txtPlotName.getText().toString());
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        Log.d("dialog", "cancel");
                        if (dialogListener != null) {
                            dialogListener.onDialogCancel();
                        }
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }

}
