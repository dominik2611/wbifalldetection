package io.dbyte.falldetection.fagments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import io.dbyte.falldetection.R;
import io.dbyte.falldetection.data.PlotStorage;
import io.dbyte.falldetection.data.entities.PlotSet;
import io.dbyte.falldetection.data.entities.StoredPlot;

import java.util.ArrayList;
import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class StoredDataFragment extends Fragment implements PlotRecyclerViewAdapter.ItemInteractionListener {


    private static final String ARG_COLUMN_COUNT = "column-count";

    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;

    private LinearLayoutManager layoutManager;

    private PlotRecyclerViewAdapter plotRecyclerViewAdapter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public StoredDataFragment() {
    }


    public static StoredDataFragment newInstance(int columnCount) {
        StoredDataFragment fragment = new StoredDataFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item_list, container, false);
        plotRecyclerViewAdapter = new PlotRecyclerViewAdapter(new ArrayList<PlotSet>(), this);

        layoutManager = new LinearLayoutManager(getContext());


        // Set the adapter

        final Context context = view.getContext();
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.savedDataList);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(plotRecyclerViewAdapter);

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                // Insert Data

                final List<PlotSet> plots = PlotStorage.getInstance(getContext()).getPlotSets();

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        plotRecyclerViewAdapter.addValues(plots);
                    }
                });

            }
        });
        recyclerView.setAdapter(plotRecyclerViewAdapter);


        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemInteraction(final PlotSet item, PlotRecyclerViewAdapter.ItemInteractionType interactionType) {
        switch (interactionType) {
            case USE:
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                PlotStorage.getInstance(getContext()).updatePlot(item);
                    }
                });
                break;
            case VIEW:
                break;
            case DELETE:
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        // Insert Data

                        PlotStorage.getInstance(getContext()).deletePlotSet(item.setId);
                        Log.d("item interaction", "deleted: " + item.setName + " ");
                        reloadData();

                    }
                });
                break;
            case NOT_USE:
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        PlotStorage.getInstance(getContext()).updatePlot(item);
                    }
                });
            default:
                break;
        }
    }

    public interface OnListFragmentInteractionListener {

        void onListFragmentInteraction(PlotSet item);
    }

    private void reloadData() {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                // Insert Data

                final List<PlotSet> plots = PlotStorage.getInstance(getContext()).getPlotSets();

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        plotRecyclerViewAdapter.clearValues();
                        plotRecyclerViewAdapter.addValues(plots);
                    }
                });

            }
        });
    }
}
