package io.dbyte.falldetection.fagments;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import io.dbyte.falldetection.R;
import io.dbyte.falldetection.data.entities.PlotSet;

import java.util.List;

public class PlotRecyclerViewAdapter extends RecyclerView.Adapter<PlotRecyclerViewAdapter.ViewHolder> {

    private final List<PlotSet> mValues;
    private final ItemInteractionListener mListener;

    public PlotRecyclerViewAdapter(List<PlotSet> items, ItemInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public PlotRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_item, parent, false);
        return new PlotRecyclerViewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PlotRecyclerViewAdapter.ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mIdView.setText(mValues.get(position).setId +"");
        holder.mContentView.setText(mValues.get(position).setName);
        holder.dateView.setText(mValues.get(position).saveDate.toString());
        holder.chkbxInUse.setChecked(mValues.get(position).inUse);
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onItemInteraction(holder.mItem, ItemInteractionType.VIEW);
                }
            }
        });


        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {

                    mListener.onItemInteraction(holder.mItem, ItemInteractionType.DELETE);
                }
            }
        });


        holder.chkbxInUse.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                holder.mItem.inUse = isChecked;
                if(isChecked){
                    mListener.onItemInteraction(holder.mItem, ItemInteractionType.USE);
                }else {
                    mListener.onItemInteraction(holder.mItem, ItemInteractionType.NOT_USE);
                }
            }
        });

    }

    public void addValues(List<PlotSet> v){
        mValues.addAll(v);
        notifyDataSetChanged();
    }

    public void clearValues(){
        mValues.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;
        public final TextView dateView;
        public PlotSet mItem;


        public TextView plotSetName;


        public CheckBox chkbxInUse;
        public Button btnDelete;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.plot_number);
            mContentView = (TextView) view.findViewById(R.id.item_content);
            plotSetName = view.findViewById(R.id.txtPlotSetName);
            chkbxInUse = view.findViewById(R.id.chkbxInUse);
            btnDelete = view.findViewById(R.id.btnDeleteEntry);
            dateView = view.findViewById(R.id.txtvDate);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }

    public interface ItemInteractionListener {
        public void onItemInteraction(PlotSet item, ItemInteractionType interactionType);
    }


    public enum ItemInteractionType {
        DELETE,
        USE,
        NOT_USE,
        VIEW
    }

}
