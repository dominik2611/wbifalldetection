package io.dbyte.falldetection.fagments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import io.dbyte.falldetection.R;
import io.dbyte.falldetection.voice.DialogThread;

import java.util.ArrayList;


public class VoiceDialogFragment extends Fragment {

   public static final boolean DBG = false; //Sichtbarkeit der Debug-Ausgabe
    private static final String TAG = "MyStt3Activity";
    public DialogThread dthread;
   public Thread dtt;

    public String str;
    public TextView mText, userText;
    public TextToSpeech tts;
    public SpeechRecognizer sr;

    public VoiceDialogFragment() {
        // Required empty public constructor
    }


    public static VoiceDialogFragment newInstance() {
        VoiceDialogFragment fragment = new VoiceDialogFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestRecordAudioPermission(); //!frägt erst wenn Sturz erkannt nach der Permission -> ändern?

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_voice_dialog, container, false);

        mText = (TextView) v.findViewById(R.id.textView1);  //Für Anzeige des erkannten Wortes, wenn Debug-Ausgabe (DBG) an
        userText = (TextView) v.findViewById(R.id.textView);  //Zusätzliche Anzeige des Sprachausgabetexts
        if(DBG) {
            mText.setVisibility(View.VISIBLE);  //Debug-Ausgabe sichtbar machen
            mText.setText("onCreate\n");
        }

      //  dthread = new DialogThread(this);
        initTTSSTT();

        return v;
    }

    private void requestRecordAudioPermission() {
        String requiredPermission = Manifest.permission.RECORD_AUDIO;
        //Wenn Berechtigung nicht erteilt wird, geht Spracheingabe nicht
        if (getContext().checkCallingOrSelfPermission(requiredPermission) == PackageManager.PERMISSION_DENIED) {
            requestPermissions(new String[]{requiredPermission}, 101);
        }
    }

    private void initTTSSTT(){
        AudioManager audioManager = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);
        audioManager.setSpeakerphoneOn(true);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC)/2,0);
        tts=new TextToSpeech(getContext(), dthread);
        sr = SpeechRecognizer.createSpeechRecognizer(getContext());
        sr.setRecognitionListener(new SpeechListener());
    }

    public void getHelp() {
        if(DBG)
            mText.append("Es wird Hilfe gerufen\n");
        userText.setText("Es wird Hilfe gerufen");
        userText.setTextColor(0xFF9CCC65);
    }

    public void onClick(View v){
        dthread.cleanup();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    class SpeechListener implements RecognitionListener
    {
        public void onReadyForSpeech(Bundle params)
        {
            Log.d(TAG, "onReadyForSpeech");
        }
        public void onBeginningOfSpeech()
        {
            Log.d(TAG, "onBeginningOfSpeech");
        }
        public void onRmsChanged(float rmsdB)
        {
            Log.d(TAG, "onRmsChanged");
        }
        public void onBufferReceived(byte[] buffer)
        {
            Log.d(TAG, "onBufferReceived");
        }
        public void onEndOfSpeech()
        {
            Log.d(TAG, "onEndofSpeech");
        }
        public void onError(int error)
        {
            Log.d(TAG,  "error " +  error);
            mText.append("fehler\n");
        }
        public void onResults(Bundle results)
        {
            String str = new String();
            Log.d(TAG, "onResults " + results);
            ArrayList data = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
            for (int i = 0; i < data.size(); i++)
            {
                Log.d(TAG, "result " + data.get(i));
                str += data.get(i);
            }
            mText.append(str+"\n"); //Anzeige des erkannten Wortes
            if(!dthread.result) {
                dthread.str = str;  //Übergabe des Ergebnisses an DialogThread
                dthread.result = true;
            }
        }
        public void onPartialResults(Bundle partialResults)
        {
            Log.d(TAG, "onPartialResults");
        }
        public void onEvent(int eventType, Bundle params)
        {
            Log.d(TAG, "onEvent " + eventType);
        }
    }

}
