package io.dbyte.falldetection.sensors;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.Looper;

public class DirectionAccProvider extends DataProviderBase implements SensorEventListener {


    private SensorManager sensorManager = null;
    private Sensor accSensor = null;

    private Sensor gravitySensor;

    private Sensor rotationSensor;

    private Sensor magneticSensor;

    private Sensor accSensor2;

    private int gravCount = 0;
    private int accCount = 0;


    float[] linAcc = new float[3];
    float[] gravity = new float[3];
    float[] magnetic = new float[3];
    float[] rotation = new float[3];

    private RotationNormalizer normalizer;


    float[] accumululatedAcc = new float[3];

    public DirectionAccProvider(Context context) {
        super(context);

        setupSensor();
    }


    @Override
    public void run() {
        normalizer = new RotationNormalizer();
        sensorManager = (SensorManager) getContext().getSystemService(Context.SENSOR_SERVICE);
        accSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        rotationSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);

        gravitySensor = sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
        magneticSensor = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

        accSensor2 = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        Looper.prepare();
        Handler gHandler = new Handler();
        //  sensorManager.registerListener(this, magneticSensor, SensorManager.SENSOR_DELAY_UI, 0);
        sensorManager.registerListener(this, accSensor, SensorManager.SENSOR_DELAY_UI, 0, gHandler);
        sensorManager.registerListener(this, gravitySensor, SensorManager.SENSOR_DELAY_UI, 0, gHandler);
        sensorManager.registerListener(this, magneticSensor, SensorManager.SENSOR_DELAY_UI, 0, gHandler);
        sensorManager.registerListener(this, accSensor2, SensorManager.SENSOR_DELAY_UI, 0, gHandler);
        Looper.loop();
    }

    private void setupSensor() {



    }


    @Override
    public void onSensorChanged(SensorEvent event) {

        if (!isReadingData()) {
            return;
        }


        if (event.sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION) {
            linAcc = event.values;

//
//            float[] accDirection = new float[4];
//            //gravity = new float[]{0,9.81f,0f};
//            accDirection = normalizer.normalize(gravity, linAcc);
//            accumululatedAcc[0] += accDirection[0];
//            accumululatedAcc[1] += accDirection[1];
//            accumululatedAcc[2] += accDirection[2];
//            //  Log.d("adass", "adsdsad "+result);
//            // Matrix.invertM(rinv, 0, r, 0);
//            // Matrix.multiplyMV(trueAcceleration, 0, rinv, 0, linAcc, 0);
//            if (sensorListener != null) {
//                sensorListener.onSensorDataArrived(accDirection);
//            }
//            // Log.d("sensor count", "acc: "+accCount  + " grav: "+gravCount);
        } else if (event.sensor.getType() == Sensor.TYPE_GRAVITY) {
            gravity = event.values;

        } else if (event.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR) {
            //  Log.d("rotation", "x: "+event.values[0]+ " y: "+event.values[1]+ " z: "+event.values[2]);
        } else if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
            magnetic = event.values;
        } else if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            float[] deviceRelativeAcceleration = new float[4];
            deviceRelativeAcceleration[0] = event.values[0];
            deviceRelativeAcceleration[1] = event.values[1];
            deviceRelativeAcceleration[2] = event.values[2];
            deviceRelativeAcceleration[3] = 0;


            float[] R = new float[16], I = new float[16], earthAcc = new float[16];

            SensorManager.getRotationMatrix(R, I, gravity, magnetic);

            float[] inv = new float[16];

            android.opengl.Matrix.invertM(inv, 0, R, 0);
            android.opengl.Matrix.multiplyMV(earthAcc, 0, inv, 0, deviceRelativeAcceleration, 0);
            //Log.d("Acceleration", "Values: (" + earthAcc[0] + ", " + earthAcc[1] + ", " + earthAcc[2] + ")");

            if (getSensorListener() != null) {

                getSensorListener().onSensorDataArrived(earthAcc);
            }

        }


    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }


}
