package io.dbyte.falldetection.sensors;

public interface IDataProvider {

    public void setSensorListener(SensorDataProvider.SensorListener sensorListener);

}
