package io.dbyte.falldetection.sensors;

import android.opengl.Matrix;
import android.util.Log;

import java.util.List;

public class RotationNormalizer {
    float[] rotMatrixX = new float[16];
    float[] rotMatrixY = new float[16];
    float[] rotMatrixZ = new float[16];


    public void printSnapshot(float[] gravityVec, float[] accVec) {


        float[] sphericalCoords = calcSphericalCoords(gravityVec);

        // rotate accVec
        float[] x = new float[]{accVec[0], 0, 0};
        float[] y = new float[]{0, accVec[1], 0};
        float[] z = new float[]{0, 0, accVec[2]};

        float[] rotX = rotateXZ(x, (float) (sphericalCoords[0] * -1.0), sphericalCoords[1]);
        float[] rotY = rotateXZ(y, (float) (sphericalCoords[0] * -1.0), sphericalCoords[1]);
        float[] rotZ = rotateXZ(z, (float) (sphericalCoords[0] * -1.0), sphericalCoords[1]);
        //    Log.d("normalizer","x "+ vec2str(rotX));
        //   Log.d("normalizer","y "+ vec2str(rotY));
        // Log.d("normalizer","z "+ vec2str(rotZ));

        float[] addition = addVecToVec(addVecToVec(rotX, rotY), rotZ);


        String gStr = vec2str(gravityVec);
        String accStr = vec2str(accVec);
        String rotatedX = vec2str(rotX);
        String rotatedY = vec2str(rotY);
        String rotatedZ = vec2str(rotZ);
        String addStr = vec2str(addition);

        Log.d("normalizer", "snapshot for g: " + gStr + " acc: " + accStr + " rotX: " + rotatedX + " rotY: " + rotatedY + " rotZ: " + rotatedZ + " norm: " + addStr);


    }

    public float[] normalize(float[] gravityVec, float[] accVec) {
        float[] sphericalCoords = calcSphericalCoords(gravityVec);

        // rotate accVec
        float[] x = new float[]{accVec[0], 0, 0};
        float[] y = new float[]{0, accVec[1], 0};
        float[] z = new float[]{0, 0, accVec[2]};

        float[] rotX = rotateXZ(x, (float) (sphericalCoords[0] * -1.0), sphericalCoords[1]);
        float[] rotY = rotateXZ(y, (float) (sphericalCoords[0] * -1.0), sphericalCoords[1]);
        float[] rotZ = rotateXZ(z, (float) (sphericalCoords[0] * -1.0), sphericalCoords[1]);
        //    Log.d("normalizer","x "+ vec2str(rotX));
        //   Log.d("normalizer","y "+ vec2str(rotY));
        // Log.d("normalizer","z "+ vec2str(rotZ));

        float[] addition = addVecToVec(addVecToVec(rotX, rotY), rotZ);


        if (accVec[0] > 1 || accVec[1] > 1 || accVec[2] > 1) {
            String gStr = vec2str(gravityVec);
            String accStr = vec2str(accVec);
            String rotatedX = vec2str(rotX);
            String rotatedY = vec2str(rotY);
            String rotatedZ = vec2str(rotZ);
            String addStr = vec2str(addition);
            Log.d("normalizer", "snapshot for g: " + gStr + " acc: " + accStr + " rotX: " + rotatedX + " rotY: " + rotatedY + " rotZ: " + rotatedZ + " norm: " + addStr);

        }
        float[] resX = calcRes(x, rotX);
        float[] resY = calcRes(y, rotY);
        float[] resZ = calcRes(z, rotZ);

        float xLen = getLength(resX);
        float yLen = getLength(resY);
        float zLen = getLength(resZ);
        float[] addition2 = addVecToVec(addVecToVec(resX, resY), resZ);
        Log.d("addition", vec2str(addition2));
        return addition2;

    }

    private float[] rotateXZ(float[] vec, float theta, float phi) {
        float[] v = new float[]{vec[0], vec[1], vec[2], 1};
        float[] result1 = new float[4];
        float[] result2 = new float[4];
        Matrix.setRotateM(rotMatrixX, 0, theta, 1, 0, 0);
        Matrix.setRotateM(rotMatrixZ, 0, phi, 0, 0, 1);

        //   Log.d("normalizer", " x: "+mat2str(rotMatrixX));
        //  Log.d("normalizer", " z: "+mat2str(rotMatrixZ));

        Matrix.multiplyMV(result1, 0, rotMatrixX, 0, v, 0);
        Matrix.multiplyMV(result2, 0, rotMatrixZ, 0, result1, 0);

        return result2;
    }

    private float[] calcRes(float[] vecA, float[] vecB){
        float[] res = new float[]{vecA[0]+vecB[0],vecA[1]+vecB[1],vecA[2]+vecB[2]};
        return res;
    }

    private float[] addVecToVec(float[] vec1, float[] vec2) {
        float[] res = new float[vec1.length];
        for (int i = 0; i < res.length; i++) {
            res[i] = vec1[i] + vec2[i];

        }

        return res;
    }


    private float getLength(float[] v) {
        return (float) Math.sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
    }

    private float[] calcSphericalCoords(float[] v) {
        float r = getLength(v);
        float theta = (float) Math.acos(v[2] / r);

        float phi = (float) Math.atan2(v[1], v[0]);

        return new float[]{radToDeg(theta, 0), radToDeg(phi, Math.PI / 2)};


    }

    private float radToDeg(double rad, double offset) {
        return (float) ((rad - offset) * (180 / Math.PI));
    }


    private String vec2str(float[] v) {
        String str = "Vector: ";
        for (int i = 0; i < v.length; i++) {
            str += "|" + v[i];

        }
        return str;
    }

    private String mat2str(float[] mat) {
        String str = "matrix: ";
        for (int i = 0; i < mat.length; i++) {
            if (i % Math.sqrt(mat.length) == 0) {

                str += "\n";
            }
            str += "|" + mat[i];

        }

        return str;
    }

}
