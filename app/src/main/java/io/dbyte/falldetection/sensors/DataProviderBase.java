package io.dbyte.falldetection.sensors;

import android.content.Context;
import android.hardware.SensorEvent;

public class DataProviderBase extends  Thread {
    private boolean isReadingData = false;
    private Context context;
    private SensorDataProvider.SensorListener sensorListener;

    public void setReadingDataEnabled(boolean enabled){
        this.isReadingData = enabled;
    }

    public DataProviderBase(Context context) {
        this.context = context;
    }

    public void setSensorListener(SensorDataProvider.SensorListener sensorListener) {
        this.sensorListener = sensorListener;
    }
    public boolean isReadingData() {
        return isReadingData;
    }

    public Context getContext() {
        return context;
    }

    public SensorDataProvider.SensorListener getSensorListener() {
        return sensorListener;
    }

    public interface SensorListener {
        public void onSensorDataArrived(float[] data);
    }

}
