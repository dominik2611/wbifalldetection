package io.dbyte.falldetection.sensors;

import android.hardware.SensorEvent;
import android.util.Log;
import com.androidplot.xy.SimpleXYSeries;
import io.dbyte.falldetection.sensors.SensorDataProvider;
import io.dbyte.falldetection.sensors.SensorPlot;

import java.util.ArrayList;
import java.util.List;

public class SensorPlotGroup implements SensorDataProvider.SensorListener, SensorPlot.PlotEventListener {


    private int dimensions;

    private ArrayList<SensorPlot> sensorPlots;

    private boolean canPanZoom = false;


    private SensorDataProvider sensorDataProvider;


    private SensorPlot.PlotEventListener sensorListener;

    private int[] savePoints = new int[2];

    public SensorPlotGroup(int dimensions) {
        this.dimensions = dimensions;
        sensorPlots = new ArrayList<>(dimensions);
    }

    public void addPlot(SensorPlot plot) {
        this.sensorPlots.add(plot);
        plot.setPlotEventListener(this);
    }

    public void setSensorDataProvider(SensorDataProvider sensorDataProvider) {
        this.sensorDataProvider = sensorDataProvider;
        sensorDataProvider.setSensorListener(this);
    }



    public void onSensorDataArrived(SensorEvent event) {
        if (event.values.length == dimensions && sensorPlots.size() == dimensions) {
            for (int i = 0; i < dimensions; i++) {
                sensorPlots.get(i).appendData(event.values[i]);
            }
        }
    }

    public void setPanZoomEnabled(boolean enabled) {
        this.canPanZoom = enabled;
        for (SensorPlot sensorPlot : sensorPlots) {
            sensorPlot.setPanZoomEnabled(enabled);
        }
    }

    public boolean canPanZoom() {
        return canPanZoom;
    }

    public void clearAll() {
        for (SensorPlot sensorPlot : sensorPlots) {
            sensorPlot.clear();
        }
    }

    public void saveData(){
        sensorDataProvider.setReadingDataEnabled(false);


        for (SensorPlot sensorPlot : sensorPlots) {
            sensorPlot.storeData(savePoints[0], savePoints[1]);
        }
        sensorDataProvider.setReadingDataEnabled(true);
    }

    @Override
    public void onStoreData(int startX, int endX) {
        sensorDataProvider.setReadingDataEnabled(false);
        for (SensorPlot sensorPlot : sensorPlots) {
            sensorPlot.storeData(startX, endX);
        }
        sensorDataProvider.setReadingDataEnabled(true);
    }

    @Override
    public void onDataStored(SimpleXYSeries storedSeries) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < storedSeries.size(); i++) {
            sb.append(" "+storedSeries.getX(i)+ " y: "+storedSeries.getY(i));
        }

        Log.d("stored data", "series: "+storedSeries.getTitle()+" vals: "+storedSeries.size());
    }

    @Override
    public void onSaveStateChanged(boolean canSave, SensorPlot sender) {
        if(sensorListener != null){
            sensorListener.onSaveStateChanged(canSave, sender);
        }

        if(canSave){
            savePoints = sender.getSavePoints();
        }
    }

    @Override
    public void onPlotClicked() {

    }

    public void setSensorListener(SensorPlot.PlotEventListener sensorListener) {
        this.sensorListener = sensorListener;
    }

    public List<SensorPlot> getSensorPlots(){
        return sensorPlots;
    }

    @Override
    public void onSensorDataArrived(float[] data) {

    }
}
