package io.dbyte.falldetection.sensors;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class SensorDataProvider extends DataProviderBase implements SensorEventListener {


    private SensorManager sensorManager = null;
    private Sensor sensor = null;


    public SensorDataProvider(Context context, int sensorType) {
        super(context);

        setupSensor(sensorType);
    }

    private void setupSensor(int sensorType) {

        sensorManager = (SensorManager) getContext().getSystemService(Context.SENSOR_SERVICE);
        sensor = sensorManager.getDefaultSensor(sensorType);

        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_UI, 0);


    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        // get rid the oldest sample in history:
        if (!isReadingData()) {
            return;
        }

        if (getSensorListener() != null) {
           getSensorListener().onSensorDataArrived(event.values);
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }


}
