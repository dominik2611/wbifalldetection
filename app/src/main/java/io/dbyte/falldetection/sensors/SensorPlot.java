package io.dbyte.falldetection.sensors;

import android.graphics.Color;
import android.graphics.PointF;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import com.androidplot.Plot;
import com.androidplot.ui.SeriesBundle;
import com.androidplot.util.Redrawer;
import com.androidplot.xy.*;
import io.dbyte.falldetection.data.entities.StoredPlot;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedList;

public class SensorPlot {

    private XYPlot plot;

    private SimpleXYSeries dataSeries[][];

    private final int HISTORY_SIZE;

    private PanZoom panZoom;

    private SimpleXYSeries selectionMarkerSeries;

    private Redrawer redrawer;


    private boolean isSelectingEndPoint = false;

    private PlotEventListener plotEventListener;

    private int[] savePoints = new int[2];

    public SensorPlot(XYPlot plot, String name, int historySize) {
        this.plot = plot;
        HISTORY_SIZE = historySize;



        plot.setRangeBoundaries(-10, 10, BoundaryMode.FIXED);
        plot.setDomainBoundaries(0, HISTORY_SIZE, BoundaryMode.FIXED);
        plot.setDomainStepMode(StepMode.INCREMENT_BY_VAL);
        plot.setDomainStepValue(HISTORY_SIZE / 10);
        plot.setLinesPerRangeLabel(3);
        plot.setRenderMode(Plot.RenderMode.USE_BACKGROUND_THREAD);

        //   plot.setDomainLabel("Sample Index");
        // plot.getDomainTitle().pack();
        // plot.setRangeLabel("Angle (Degs)");
        //   plot.getRangeTitle().pack();

        plot.getGraph().getLineLabelStyle(XYGraphWidget.Edge.LEFT).
                setFormat(new DecimalFormat("#"));

        plot.getGraph().getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).
                setFormat(new DecimalFormat("#"));

        redrawer = new Redrawer(plot, 100, true);

        panZoom = setupPanZoom(plot);

    }

    public XYPlot getPlot(){
        return plot;
    }


    public void setPlotEventListener(PlotEventListener plotEventListener) {
        this.plotEventListener = plotEventListener;
        if(plotEventListener != null){
            plotEventListener.onSaveStateChanged(false, this);

        }
    }

    private PanZoom setupPanZoom(final XYPlot plot) {
        final PanZoom panZoom = PanZoom.attach(plot, PanZoom.Pan.BOTH, PanZoom.Zoom.STRETCH_BOTH, PanZoom.ZoomLimit.MIN_TICKS);


        panZoom.setDelegate(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if(plotEventListener != null){
                        plotEventListener.onPlotClicked();
                    }
                    //  onPlotClicked(new PointF(motionEvent.getX(), motionEvent.getY()), (XYPlot) view);
                } else if (motionEvent.getAction() == MotionEvent.ACTION_MOVE) {

                    placeCursor(new PointF(motionEvent.getX(), motionEvent.getY()), (XYPlot) view);
                } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    onPlotUp(new PointF(motionEvent.getX(), motionEvent.getY()), (XYPlot) view);

                }
                return false;
            }
        });

        return panZoom;
    }


    private void placeCursor(PointF point, XYPlot plot) {
        if (plot.containsPoint(point.x, point.y)) {
            plot.setCursorPosition(point);
        }
    }

    private void onPlotUp(PointF point, XYPlot plot) {
        if (plot.containsPoint(point.x, point.y)) {
            Number x = plot.getXVal(point);
            Number y = plot.getYVal(point);

            Log.d("up", "on plot: " + point.x + " y: " + point.y + " nr x: " + x + " nr y: " + y);
            for (SeriesBundle<XYSeries, ? extends XYSeriesFormatter> sfPair : plot
                    .getRegistry().getSeriesAndFormatterList()) {
                XYSeries series = sfPair.getSeries();
                Log.d("serie", "size: " + series.size());

                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < series.size(); i++) {
                    Number thisX = series.getX(i);
                    Number thisY = series.getY(i);
                    sb.append(i + ": x: " + thisX + " y: " + thisY + " ");
                    if (i % 5 == 0) {
                        sb.append("\n");
                    }
                    float diff = x.floatValue() - series.getX(i).floatValue();

                    if (diff <= 1 && diff >= 0) {
                        Log.d("clicked point: ", "x: " + series.getX(i) + " y: " + series.getY(i));
                        setMarker(series.getX(i).intValue());
                        plot.redraw();
                        return;
                    }
                }
                //  Log.d("series", "points: "+sb.toString());

            }

        }
    }

    private void setMarker(int xPos) {

        if (selectionMarkerSeries == null) {
            selectionMarkerSeries = new SimpleXYSeries("Selection");
            plot.addSeries(selectionMarkerSeries, new BarFormatter(Color.BLUE, Color.GREEN));
        }

        if (!isSelectingEndPoint) {
            selectionMarkerSeries.clear();
            selectionMarkerSeries.addFirst(xPos, 30);
            isSelectingEndPoint = true;
            Log.d("selection", "START POINT");
            if(plotEventListener != null){
                plotEventListener.onSaveStateChanged(false, this);

            }
        } else {
            selectionMarkerSeries.addLast(xPos, 30);
            isSelectingEndPoint = false;
            LinkedList<Number> selectionPoints = selectionMarkerSeries.getxVals();
            Log.d("selection", "END POINT");
            if (selectionPoints.size() > 1) {
                // storeSeries(selectionPoints.get(0).intValue(), selectionPoints.get(1).intValue());
                if(plotEventListener != null){
                    plotEventListener.onSaveStateChanged(true, this);

                }

                savePoints = new int[]{selectionPoints.get(0).intValue(), selectionPoints.get(1).intValue()};
            }
        }


    }

    public void clearMarker(){
        selectionMarkerSeries.clear();
    }

    public StoredPlot prepareSave(){
//        ArrayList<Float> yValues = new ArrayList<>();
//        for (Number val : dataSeries.getyVals()) {
//            yValues.add(val.floatValue());
//        }
//
//        StoredPlot sp = new StoredPlot(dataSeries.getTitle(),yValues);
//        return sp;

        return null;
    }

    public void setPanZoomEnabled(boolean enabled) {
        panZoom.setEnabled(enabled);
    }

    public void appendData(float value) {
//        if (dataSeries.size() > HISTORY_SIZE) {
//            dataSeries.removeFirst();
//
//        }
//        dataSeries.addLast(null, value);
        plot.redraw();
    }

    public void clear() {
        //dataSeries.clear();
    }



    public SimpleXYSeries storeData(int startX, int endX) {
//        SimpleXYSeries storeSeries = new SimpleXYSeries("Stored "+dataSeries.getTitle());
//        for (int i = startX; i < endX; i++) {
//            Number x = dataSeries.getX(i);
//            Number y = dataSeries.getY(i);
//            storeSeries.addLast(x,y);
//        }
//
//        if(plotEventListener != null){
//            plotEventListener.onDataStored(storeSeries);
//        }
//        return storeSeries;

        return null;

    }

    public int[] getSavePoints() {
        return savePoints;
    }

    public interface PlotEventListener {
        public void onStoreData(int startX, int endX);

        public void onDataStored(SimpleXYSeries storedSeries);

        public void onSaveStateChanged(boolean canSave, SensorPlot sender);

        public void onPlotClicked();
    }

}
