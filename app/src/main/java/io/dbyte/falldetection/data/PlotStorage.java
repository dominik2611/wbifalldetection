package io.dbyte.falldetection.data;

import android.content.Context;
import android.util.Log;
import androidx.room.Database;
import androidx.room.Room;
import io.dbyte.falldetection.data.entities.StoredPlot;
import io.dbyte.falldetection.data.entities.PlotSet;

import java.util.ArrayList;
import java.util.List;

public class PlotStorage {



    private static PlotStorage instance;

    private PlotDatabase db;

    private Context context;

    private PlotStorage(Context context){



      db = Room.databaseBuilder(context,
                PlotDatabase.class, "database-plots").fallbackToDestructiveMigration().build();
    this.context = context;
    }


    public static  PlotStorage getInstance(Context context){
        if(instance == null){
            instance = new PlotStorage(context);
        }
        return instance;
    }

    public void storePlot(PlotSet plotSet){
        Log.d("store", "new plot "+plotSet.storedPlots.size());
       // PlotSet plotSet = new PlotSet();

        db.plotDao().insertPlotSetWithPlots(plotSet);
    }


    public List<PlotSet> getPlotSets(){
        Log.d("store", "get plot");
        return db.plotDao().getAllPlotSetWithPlots();
    }

    public void clear(){
        db.plotDao().deleteAll();
    }


    public void deletePlotSet(long psId) {

      db.plotDao().delete(psId);
    }

    public void updatePlot(PlotSet plotSet) {
        Log.d("update", plotSet.setName + " "+plotSet.inUse);
        db.plotDao().updatePlotSet(plotSet);
    }




}
