package io.dbyte.falldetection.data;

import android.content.Context;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import io.dbyte.falldetection.data.entities.StoredPlot;
import io.dbyte.falldetection.data.entities.PlotSet;


@Database(entities = {PlotSet.class, StoredPlot.class}, version = 4)
@TypeConverters({Converters.class})
public abstract class PlotDatabase extends RoomDatabase {







    public abstract PlotDao plotDao();
}
