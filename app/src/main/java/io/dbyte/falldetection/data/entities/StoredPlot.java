package io.dbyte.falldetection.data.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.ArrayList;

@Entity
public class StoredPlot {

    @PrimaryKey(autoGenerate = true)
    public int uid;


    @ColumnInfo(name="plotSetId")
    public long plotSetId;

    @ColumnInfo(name = "name")
    public String plotName;

    @ColumnInfo(name = "yValues")
    public ArrayList<Float> yValues;

    public StoredPlot(String plotName, ArrayList<Float> yValues) {

        this.plotName = plotName;
        this.yValues = yValues;
    }
}
