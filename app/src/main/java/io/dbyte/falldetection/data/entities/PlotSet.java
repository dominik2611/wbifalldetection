package io.dbyte.falldetection.data.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.Date;
import java.util.List;

@Entity
public class PlotSet {

    @PrimaryKey(autoGenerate = true)
    public long setId;


    @ColumnInfo(name = "name")
    public String setName;

    @ColumnInfo(name="saveDate")
    public Date saveDate;

    @ColumnInfo(name="inUse")
    public boolean inUse;


    @Ignore
    public List<StoredPlot> storedPlots;

    public PlotSet(){

    }

    public PlotSet(int uid, String name, Date date){
        this.setName = name;
        this.setId = uid;
        this.saveDate = date;
        this.inUse = false;
    }

    @Override
    public String toString() {

        String p = "";


        return "Plot: uid: "+ setId + " name: "+ setName;
    }
}
