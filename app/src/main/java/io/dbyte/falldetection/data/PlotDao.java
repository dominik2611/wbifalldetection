package io.dbyte.falldetection.data;

import android.util.Log;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import io.dbyte.falldetection.data.entities.StoredPlot;
import io.dbyte.falldetection.data.entities.PlotSet;

import java.util.List;

@Dao
public abstract class PlotDao {

    @Insert
    public abstract long insertPlotSet(PlotSet plotSet);


    @Query("SELECT * FROM PlotSet")
    public abstract List<PlotSet> getPlotSets();

    @Query("DELETE FROM PlotSet")
    public abstract void deleteAll();


    @Query("DELETE FROM PlotSet WHERE setId =:id")
    public abstract void delete(long id);


    @Query("SELECT * FROM StoredPlot WHERE plotSetId =:psId" )
    public abstract List<StoredPlot> getPlotList(long psId);

    @Update
    public abstract int updatePlotSet(PlotSet plotSet);

     @Insert
     public abstract List<Long> insertPlotList(List<StoredPlot> storedPlot);


    public void insertPlotSetWithPlots(PlotSet plotSet) {
        long newPlotId = insertPlotSet(plotSet);
        List<StoredPlot> storedPlots = plotSet.storedPlots;
        for (int i = 0; i < storedPlots.size(); i++) {
            storedPlots.get(i).plotSetId = newPlotId;
        }
      insertPlotList(storedPlots);

    }

    public List<PlotSet> getAllPlotSetWithPlots(){
        List<PlotSet> plotSets = getPlotSets();
        for (PlotSet plotSet : plotSets) {
            List<StoredPlot> storedPlotList = getPlotList(plotSet.setId);
            plotSet.storedPlots = storedPlotList;
            Log.d("get plot", plotSet+"count of plotlist: "+storedPlotList.size() );
        }



        return plotSets;

    }

}
