package io.dbyte.falldetection;


import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;
import io.dbyte.falldetection.data.entities.PlotSet;
import io.dbyte.falldetection.dummy.SensorTest;
import io.dbyte.falldetection.fagments.*;

public class MainActivity extends AppCompatActivity implements SensorTest.OnFragmentInteractionListener, AccelerationSensor.OnFragmentInteractionListener, StoredDataFragment.OnListFragmentInteractionListener {


    private TextView mTextMessage;

    long startTime = 0;


    Handler timerHandler = new Handler();
    Runnable timerRunnable = new Runnable() {

        @Override
        public void run() {
            long millis = System.currentTimeMillis() - startTime;
            int seconds = (int) (millis / 1000);
            int minutes = seconds / 60;
            seconds = seconds % 60;

            //timerTextView.setText(String.format("%d:%02d", minutes, seconds));
            Log.d("timer", "called");

            timerHandler.postDelayed(this, 1500);
        }
    };


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Fragment overviewFragment = OverviewFragment.newInstance();
                    openFragment(overviewFragment);
                 //   mTextMessage.setText(R.string.title_home);
                    return true;

                case R.id.navigation_stored_data:
                    Fragment storedData = StoredDataFragment.newInstance(3);
                    openFragment(storedData);
                 //   mTextMessage.setText(R.string.title_stored_data);
                    return true;

            }
            return false;
        }
    };


    private void openFragment(Fragment fragment) {

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        Fragment overviewFragment = OverviewFragment.newInstance();
        openFragment(overviewFragment);

        //  timerHandler.postDelayed(timerRunnable, 0);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // getMenuInflater().inflate(R.menu.only_github, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

//        switch (item.getItemId()) {
//            case R.id.viewGithub: {
//                Intent i = new Intent(Intent.ACTION_VIEW);
//                i.setData(Uri.parse("https://github.com/PhilJay/MPAndroidChart/blob/master/MPChartExample/src/com/xxmassdeveloper/mpchartexample/LineChartActivityColored.java"));
//                startActivity(i);
//                break;
//            }
//        }

        return true;
    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }



    @Override
    public void onListFragmentInteraction(PlotSet item) {

    }
}
