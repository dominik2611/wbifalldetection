package io.dbyte.falldetection.voice;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class VoiceManager {

    static final boolean DBG = false;

    DialogThread dthread;
    Thread dtt;
    public TextToSpeech tts;
    public SpeechRecognizer sr;


    private Context context;

    private Fragment parentFragment;

    public VoiceManager(Context context, Fragment parentFragment){
        this.context = context;
        this.parentFragment = parentFragment;
    }


    public void start(){


        dthread = new DialogThread(this);
        initTTSSTT();
    }


    public Fragment getParentFragment() {
        return parentFragment;
    }

    public Context getContext() {
        return context;
    }

    private void initTTSSTT(){
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        audioManager.setSpeakerphoneOn(true);   //Ausgabe auf Lautsprecher
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC)/2,0); //halbe Lautstärke
        tts=new TextToSpeech(context, dthread);
        sr = SpeechRecognizer.createSpeechRecognizer(context);
        sr.setRecognitionListener(new listener());
    }

    public void getHelp() {
        if(DBG) {
            //  mText.append("Es wird Hilfe gerufen\n");
            //userText.setText("Es wird Hilfe gerufen");
            //userText.setTextColor(0xFF9CCC65);
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_CALL);
            intent.setData(Uri.parse("tel:004972256072008")); //Telefonnummer hardcoded-
            context.startActivity(intent);
        }

    }

    class listener implements RecognitionListener
    {
        public void onReadyForSpeech(Bundle params)
        {
            Log.d(TAG, "onReadyForSpeech");
        }
        public void onBeginningOfSpeech()
        {
            Log.d(TAG, "onBeginningOfSpeech");
        }
        public void onRmsChanged(float rmsdB)
        {
            Log.d(TAG, "onRmsChanged");
        }
        public void onBufferReceived(byte[] buffer)
        {
            Log.d(TAG, "onBufferReceived");
        }
        public void onEndOfSpeech()
        {
            Log.d(TAG, "onEndofSpeech");
        }
        public void onError(int error)
        {
            Log.d(TAG,  "error " +  error);
           // mText.append("fehler\n");
            if(error == 6){     //error 6: Speech timeout while Audio recording
                dthread.result = true;  //wir simulieren einen leeren gesprochenen Satz
                dthread.str = "";
            }
        }
        public void onResults(Bundle results)
        {
            String str = new String();
            Log.d(TAG, "onResults " + results);
            ArrayList data = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
            for (int i = 0; i < data.size(); i++)
            {
                Log.d(TAG, "result " + data.get(i));
                str += data.get(i);
            }
            //mText.append(str+"\n");
            if(!dthread.result) {
                dthread.str = str;
                dthread.result = true;
            }
        }
        public void onPartialResults(Bundle partialResults)
        {
            Log.d(TAG, "onPartialResults");
        }
        public void onEvent(int eventType, Bundle params)
        {
            Log.d(TAG, "onEvent " + eventType);
        }
    }

}
