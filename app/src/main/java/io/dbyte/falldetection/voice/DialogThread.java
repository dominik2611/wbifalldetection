package io.dbyte.falldetection.voice;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.speech.tts.Voice;
import androidx.fragment.app.Fragment;
import io.dbyte.falldetection.fagments.DialogFlowFragment;
import io.dbyte.falldetection.fagments.VoiceDialogFragment;

import java.util.Locale;

public class DialogThread implements Runnable, TextToSpeech.OnInitListener {

   public VoiceManager activity;
    public boolean result = false;
    public boolean needHelp = false;
    public String str;
    static boolean IS_STARTED = false;
    public DialogThread(VoiceManager manager) {
        this.activity =  manager;
    }

    public void start() {
        if(! IS_STARTED) {
            IS_STARTED = true;
            activity.dtt = new Thread(this);
            activity.dtt.start();
        }
    }

    public void onInit(int status) {
        if (status != TextToSpeech.ERROR) {
            d("TTS Initialization success!");
            activity.tts.setLanguage(Locale.getDefault());
            activity.dthread.start();
        }
    }

    @Override
    public void run() {
        if (startDialog()) {
            activity.tts.speak("Es wird Hilfe gerufen.", TextToSpeech.QUEUE_FLUSH, null);
            waitForSpeechEnd();
            activity.getHelp();
        }else{
            activity.tts.speak("Es wird keine Hilfe gerufen.", TextToSpeech.QUEUE_FLUSH, null);
            waitForSpeechEnd();
        }
        cleanup();
       // activity.finish();
        IS_STARTED = false;
    }

    public void cleanup() {
        activity.tts.shutdown();
        activity.getParentFragment().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                activity.sr.destroy();
            }
        });
    }

    private boolean startDialog() {
        boolean getHelp = false;
        int i = 0;
        while (i < 3) {
            i++;
            askQuestion();
            boolean r = getResponse();
            if (r) {
                if (needHelp)
                    return true;
                return false;
            }
        }
        return true;
    }

    private void askQuestion() {
        activity.tts.speak("Benötigen Sie Hilfe? Antworten Sie mit Ja oder Nein", TextToSpeech.QUEUE_FLUSH, null);
        d("labert\n");
        waitForSpeechEnd();
    }

    private void waitForSpeechEnd(){
        sleep(3000);
        d("...");
        while (activity.tts.isSpeaking()) {
            sleep(100);
        }
    }

    private boolean getResponse() {
        final Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, activity.getParentFragment().getActivity().getPackageName());
        intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 3);
        d("startet zuhören\n");
        activity.getParentFragment().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                activity.sr.startListening(intent);
            }
        });
        d("hört zu\n");
        result = false;
        int cntr = 0;
        while (cntr < 100) {
            if (result)
                break;
            sleep(100);
            cntr ++;
        }
        activity.getParentFragment().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(!result)
                    activity.sr.stopListening();
            }
        });
        d("Zeit vorbei\n");
        sleep(100);
        if (result && str != null) {
            boolean detected = false;
            if (str.matches(".*[Jj]a.*")) { // Groß- und Kleinschreibung, weil Spracherkennung große und kleine Anfangsbuchstaben liefert
                detected = true;
                needHelp = true;
            }
            if (str.matches(".*[Nn]ein.*")) {
                detected = true;
                needHelp = false;
            }
            if (detected)
                return true;
        }
        return false;
    }

    public void d(final String s) {
        if (VoiceManager.DBG)
            activity.getParentFragment().getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //activity.mText.append(s + "\n");
                }
            });
    }

    private void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
