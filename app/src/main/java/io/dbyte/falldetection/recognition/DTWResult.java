package io.dbyte.falldetection.recognition;

public class DTWResult {

    private double distance;
    private int[][] warpingPath;

    public DTWResult(double distance, int[][] warpingPath) {
        this.distance = distance;
        this.warpingPath = warpingPath;
    }

    public double getDistance() {
        return distance;
    }

    public int[][] getWarpingPath() {
        return warpingPath;
    }

    @Override
    public String toString(){
        String retVal = "Warping Distance: " + distance+ "\n";
//        retVal += "Warping Path: {";
//        for (int i = 0; i < warpingPath.length; i++) {
//            retVal += "(" + warpingPath[i][0] + ", " + warpingPath[i][1] + ")";
//            retVal += (i == warpingPath.length - 1) ? "}" : ", ";
//
//        }
        return retVal;
    }

}
