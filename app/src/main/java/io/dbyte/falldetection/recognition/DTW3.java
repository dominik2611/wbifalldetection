package io.dbyte.falldetection.recognition;

import java.util.ArrayList;
import java.util.Collections;

public class DTW3 {
//    // Size of obeservations vectors.
//    int dim;
//
//    // Known sequences
//    ArrayList<Float> sequences;
//
//    // Labels of those known sequences
//    ArrayList labels;
//
//    // Maximum DTW distance between an example and a sequence being classified.
//    double globalThreshold;
//
//    // Maximum distance between the last observations of each sequence.
//    double firstThreshold;
//
//    // Maximum vertical or horizontal steps in a row.
//    int maxSlope;
//
//    public DTW3(int dim, double threshold, double firstThreshold) {
//        this.dim = dim;
//        sequences = new ArrayList();
//        labels = new ArrayList();
//        this.globalThreshold = threshold;
//        this.firstThreshold = firstThreshold;
//        this.maxSlope = Integer.MAX_VALUE;
//    }
//
//    public DTW3(int dim, double threshold, double firstThreshold, int ms) {
//        this.dim = dim;
//        sequences = new ArrayList();
//        labels = new ArrayList();
//        this.globalThreshold = threshold;
//        this.firstThreshold = firstThreshold;
//        this.maxSlope = ms;
//    }
//
//    // Add a seqence with a label to the known sequences library.
//    // The gesture MUST start on the first observation of the sequence and end on the last one.
//    // Sequences may have different lengths.
//    public void Add(ArrayList<Float> seq, String l) {
//        sequences.addAll(seq);
//        labels.add(l);
//    }
//
//    // Recognize gesture in the given sequence.
//    // It will always assume that the gesture ends on the last observation of that sequence.
//    // If the distance between the last observations of each sequence is too great, or if the overall DTW distance between the two sequence is too great, no gesture will be recognized.
//    public String recognize(ArrayList seq) {
//        double minDist = Double.POSITIVE_INFINITY;
//        String _class = "__UNKNOWN";
//        for (int i = 0; i < sequences.size(); i++) {
//            ArrayList example = (ArrayList) sequences[i];
//            if (dist2((double[]) seq[seq.size() - 1], (double[]) example[example.size() - 1]) < firstThreshold) {
//                double d = dtw(seq, example) / (example.size());
//                if (d < minDist) {
//                    minDist = d;
//                    _class = (String) (labels[i]);
//                }
//            }
//        }
//        return (minDist < globalThreshold ? _class : "__UNKNOWN") + " "/*+minDist.ToString()*/;
//    }
//
//
//    // Computes a 1-distance between two observations. (aka Manhattan distance).
//    private double dist1(double[] a, double[] b) {
//        double d = 0;
//        for (int i = 0; i < dim; i++) {
//            d += Math.abs(a[i] - b[i]);
//        }
//        return d;
//    }
//
//    // Computes a 2-distance between two observations. (aka Euclidian distance).
//    private double dist2(double[] a, double[] b) {
//        double d = 0;
//        for (int i = 0; i < dim; i++) {
//            d += Math.pow(a[i] - b[i], 2);
//        }
//        return Math.sqrt(d);
//    }
//
//    // Compute the min DTW distance between seq2 and all possible endings of seq1.
//    public double dtw(ArrayList seq1, ArrayList seq2) {
//        // Init
//        ArrayList<Float> seq1r = new ArrayList(seq1);
//        Collections.reverse(seq1r);
//
//        ArrayList<Float> seq2r = new ArrayList(seq2);
//        Collections.reverse(seq2r);
//        double[][] tab = new double[seq1r.size() + 1][seq2r.size() + 1];
//        int[][] slopeI = new int[seq1r.size() + 1][seq2r.size() + 1];
//        int[][] slopeJ = new int[seq1r.size() + 1][seq2r.size() + 1];
//
//        for (int i = 0; i < seq1r.size() + 1; i++) {
//            for (int j = 0; j < seq2r.size() + 1; j++) {
//                tab[i][j] = Double.POSITIVE_INFINITY;
//                slopeI[i][j] = 0;
//                slopeJ[i][j] = 0;
//            }
//        }
//        tab[0][0] = 0;
//
//        // Dynamic computation of the DTW matrix.
//        for (int i = 1; i < seq1r.size() + 1; i++) {
//            for (int j = 1; j < seq2r.size() + 1; j++) {
//                if (tab[i][j - 1] < tab[i - 1][j - 1] && tab[i][j - 1] < tab[i - 1][j] && slopeI[i][j - 1] < maxSlope) {
//                    tab[i][j] = dist2((double[]) seq1r.get(i - 1), (double[]) seq2r.get(j - 1)) + tab[i, j - 1];
//                    slopeI[i, j] =slopeJ[i, j - 1]+1;
//                    ;
//                    slopeJ[i, j] =0;
//                } else if (tab[i - 1,j] <tab[i - 1, j - 1] &&tab[i - 1, j] <tab[i, j - 1] &&slopeJ[i - 1, j] <maxSlope)
//                {
//                    tab[i, j] =dist2((double[]) seq1r[i - 1], (double[]) seq2r[j - 1]) + tab[i - 1, j];
//                    slopeI[i, j] =0;
//                    slopeJ[i, j] =slopeJ[i - 1, j]+1;
//                }
//          else
//                {
//                    tab[i, j] =dist2((double[]) seq1r[i - 1], (double[]) seq2r[j - 1]) + tab[i - 1, j - 1];
//                    slopeI[i, j] =0;
//                    slopeJ[i, j] =0;
//                }
//            }
//        }
//
//        // Find best between seq2 and an ending (postfix) of seq1.
//        double bestMatch = double.PositiveInfinity;
//        for (int i = 1; i < seq1r.size() + 1; i++) {
//            if (tab[i,seq2r.size()] <bestMatch)
//            bestMatch = tab[i, seq2r.size()];
//        }
//        return bestMatch;
//    }
}
