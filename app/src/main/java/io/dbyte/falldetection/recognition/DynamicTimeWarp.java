package io.dbyte.falldetection.recognition;

public class DynamicTimeWarp {

    private double[] referenceGesture;


    public DynamicTimeWarp(){


    }


    public void setReferenceGesture(double[] referenceGesture) {
        this.referenceGesture = referenceGesture;
    }

    public DTWResult detect(double[] inputData){
        double accumulatedDistance = 0.0;
        double[] seqRef = referenceGesture;
        double[] seqTest = inputData;

        double warpingDistance = 0;


        int[][] warpPath = new int[seqRef.length + seqTest.length][2];

        double[][] localDistance = new double[seqRef.length][seqTest.length];
        double[][] globalDistance = new double[seqRef.length][seqTest.length];

        for (int i = 0; i < seqRef.length; i++) {
            for (int j = 0; j < seqTest.length; j++) {
                localDistance[i][j] = distanceBetween(seqRef[i], seqTest[j]);
            }
        }

        globalDistance[0][0] = localDistance[0][0];

        for (int i = 1; i < seqRef.length; i++) {
            globalDistance[i][0] = localDistance[i][0] + globalDistance[i-1][0];
        }

        for (int j = 1; j < seqTest.length; j++) {
            globalDistance[0][j] = localDistance[0][j] + globalDistance[0][j-1];
        }


        for (int i = 1; i < seqRef.length; i++) {
            for (int j = 1; j < seqTest.length; j++) {
                accumulatedDistance = Math.min(Math.min(globalDistance[i - 1][j], globalDistance[i - 1][j - 1]), globalDistance[i][j - 1]);
                accumulatedDistance += localDistance[i][j];
                globalDistance[i][j] = accumulatedDistance;
            }

        }

        accumulatedDistance = globalDistance[seqRef.length - 1][seqTest.length - 1];

        int i = seqRef.length - 1;
        int j = seqTest.length - 1;
        int minIndex = 1;

        int K =  1;

        warpPath[K - 1][0] = i;
        warpPath[K - 1][1] = j;

        while ((i + j) != 0) {
            if (i == 0) {
                j -= 1;
            } else if (j == 0) {
                i -= 1;
            } else {        // i != 0 && j != 0
                double[] array = { globalDistance[i - 1][j], globalDistance[i][j - 1], globalDistance[i - 1][j - 1] };
                minIndex = this.getIndexOfMinimum(array);

                if (minIndex == 0) {
                    i -= 1;
                } else if (minIndex == 1) {
                    j -= 1;
                } else if (minIndex == 2) {
                    i -= 1;
                    j -= 1;
                }
            } // end else
            K++;
            warpPath[K - 1][0] = i;
            warpPath[K - 1][1] = j;
        } // end while
        warpingDistance = accumulatedDistance / K;

        int[][] reversedWarpPath = this.reversePath(warpPath, K);
        DTWResult res = new DTWResult(warpingDistance, reversedWarpPath);

        return res;
        //   printPath(warpingDistance, K, reversedWarpPath);
    }



    protected int[][] reversePath(int[][] path, int k) {
        int[][] newPath = new int[k][2];
        for (int i = 0; i < k; i++) {
            for (int j = 0; j < 2; j++) {
                newPath[i][j] = path[k - i - 1][j];
            }
        }
        return newPath;
    }

    protected int getIndexOfMinimum(double[] array) {
        int index = 0;
        double val = array[0];

        for (int i = 1; i < array.length; i++) {
            if (array[i] < val) {
                val = array[i];
                index = i;
            }
        }
        return index;
    }

    protected double distanceBetween(double p1, double p2) {
        return (p1 - p2) * (p1 - p2);
    }

}
