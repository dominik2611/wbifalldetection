package io.dbyte.falldetection.recognition;

import android.util.Log;

public class SimpleRecognizer {

    private RingBuffer<Float> buffer;

    private double[] intervallBuffer;

    private int newValueCount = 0;

    private int detectionIntervallSize = 20;

    private FallDetectedListener fallDetectedListener;

    public SimpleRecognizer(int bufferSize) {
        buffer = new RingBuffer<Float>(bufferSize, Float.class);
        intervallBuffer = new double[20];
    }


    public double addSensorDataFrame(float[] values) {
        // calculate absolute value

        double absoluteValue = calcAbsoluteValue(values);
        intervallBuffer[newValueCount] = absoluteValue;
        newValueCount++;

        if (newValueCount >= detectionIntervallSize) {
            detect();
        }

        if (newValueCount >= intervallBuffer.length) {
            newValueCount = 10;
            // reuse last 10 values
            int startIndex = newValueCount - 1;
            for (int i = startIndex; i < intervallBuffer.length; i++) {
                intervallBuffer[i - startIndex] = intervallBuffer[i];

            }
        }
        return absoluteValue;
    }

    public double[] getInternalData() {
        double[] vals = new double[2];
        vals[0] = calcAverage(intervallBuffer);
        vals[1] = calcVariance(intervallBuffer, vals[0]);
        return vals;
    }


    private void detect() {
        double average = calcAverage(intervallBuffer);
        double variance = calcVariance(intervallBuffer, average);
     //   Log.d("simple recognizer:", "avg: " + average + " v: " + variance);
        if(average > 0.9 && variance >= 0.25){
            if(fallDetectedListener != null){
                fallDetectedListener.onFallDetected("detected with greater 0.9");

            }
            Log.d("simple recognizer", "detected with greater 0.9");
            Log.d("simple recognizer:", "avg: " + average + " v: " + variance);
        }else if(average < 0.9 && variance > 0.1){
           // Log.d("simple recognizer", "detected with less than 0.9");
//            if(fallDetectedListener != null){
//                fallDetectedListener.onFallDetected("detected with less than 0.9");
//                Log.d("simple recognizer:", "avg: " + average + " v: " + variance);
//            }
        }

    }

    private double calcAbsoluteValue(float[] values) {
        double abs = 0;
        double a = 0;
        for (int i = 0; i < values.length; i++) {
            a = values[i] * values[i];
        }
        abs = Math.sqrt(a);
        return abs;
    }

    private double calcAverage(double[] v) {
        double m = 0;
        for (int i = 0; i < v.length; i++) {
            m += v[i];

        }

        return m / v.length;
    }

    private double calcVariance(double[] v, double avg) {
        double var = 0;
        for (int i = 0; i < v.length; i++) {
            var += (v[i] - avg) * (v[i] - avg);
        }
        return var / (v.length - 1.0);
    }

    public void setFallDetectedListener(FallDetectedListener fallDetectedListener) {
        this.fallDetectedListener = fallDetectedListener;
    }


    public interface FallDetectedListener {
        void onFallDetected(String status);
    }

}
