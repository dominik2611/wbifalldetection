package io.dbyte.falldetection.recognition;

import androidx.annotation.NonNull;

public class DTWValue {

        public float[] values;

        public DTWValue(float[] values){
            this.values = values;
        }

        public double distance(DTWValue v){

            double f = 0;
            for (int i = 0; i < values.length; i++) {
                f+=(values[i]-v.values[i])*(values[i]-v.values[i]);

            }
            return Math.sqrt(f);
        }


    @Override
    public String toString() {
        String s = "(";
        for (int i = 0; i < values.length; i++) {
            s+= "|"+values[i];
        }
        s += ")";
        return s;
    }
}
