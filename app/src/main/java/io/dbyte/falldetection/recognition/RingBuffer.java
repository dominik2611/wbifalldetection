package io.dbyte.falldetection.recognition;

import java.lang.reflect.Array;

public class RingBuffer<T> {

    private int maxCapacity;

    private int readPos;

    private int writePos;

    private int currentStoredElements;

    private T[] data;


    private boolean isOverflow = false; // used to determine whether the array length was exceeded and writing starts at beginning

    public RingBuffer(int maxCapacity, Class<T> dataType) {
        this.maxCapacity = maxCapacity;
        writePos = 0;
        data = (T[]) Array.newInstance(dataType, maxCapacity);
        readPos = 0;

    }

    public boolean store(T el) {

        if(isOverflow){
            if(writePos < readPos){
                data[writePos] = el;
                writePos++;
                return true;
            } else {
                return false;
            }
        }else {
            if (writePos >= data.length) {
                // start at beginning of buffer
                writePos = 0;
                isOverflow = true;
                if(writePos < readPos){
                    data[writePos] = el;
                    writePos++;
                    return true;
                } else {
                    return false;
                }
            }else {
                data[writePos] = el;
                writePos++;
                return true;
            }

        }


    }


    public T getNext() {
        // read the next element
        // can only read, where something was written
        if (isOverflow) {
            if (readPos == maxCapacity) {
                readPos = 0;
                isOverflow = false;

                if (readPos < writePos) {
                    T el = data[readPos];
                    readPos++;
                    return el;
                } else {
                    return null;
                }
            } else {
                T el = data[readPos];
                readPos++;
                return el;
            }
        } else {
            if (readPos < writePos) {
                T el = data[readPos];
                readPos++;
                return el;

            } else {
                return null;
            }
        }


    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < data.length; i++) {
            T d = data[i];
            sb.append(" | "+d);
        }
        return sb.toString();
    }

}
