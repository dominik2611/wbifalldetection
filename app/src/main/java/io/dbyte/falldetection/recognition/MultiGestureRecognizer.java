package io.dbyte.falldetection.recognition;

import android.util.Log;

import java.util.LinkedList;

public class MultiGestureRecognizer {


    private DTWValue[] reference;


    private LinkedList<DTWValue> continouusBuffer;
    private int continouusBufferSize;

    private DynamicTimeWarp2 dtw;


    private int minTestValueCount; // minimal amount of test values to check gesture

    private double threshold;

    private GestureRecognizedListener gestureRecognizedListener;

    private String name;

    public void setName(String name) {
        this.name = name;
    }

    private int detectionIntervall = 20;

    private int id = 0;

    public MultiGestureRecognizer(int id, String name, DTWValue[] reference, int minTestValueCount, double threshold) {
        this.reference = reference;


        dtw = new DynamicTimeWarp2();
        dtw.setReferenceGesture(reference);
        String ref = "";
        for (int i = 0; i < reference.length; i++) {
            DTWValue dtwValue = reference[i];
            for (float v : dtwValue.values) {
                ref+="|"+v;
            }

        }

        Log.d("dtw reference", ref);



//        printDTWValues("reference", reference);

        this.minTestValueCount = minTestValueCount;
        this.threshold = threshold;
        this.name = name;
        this.id = id;
        continouusBuffer = new LinkedList<>();
        continouusBufferSize = reference.length * 2;
        // continousBuffer = new RingBuffer<Float>(reference.length *2, Float.class);

    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public void addToBuffer(DTWValue value) {

      //  printDTWValues("add", new DTWValue[]{value});
      //  Log.d("buffer", "max: "+continouusBufferSize + " current: "+continouusBuffer.size());
        if (continouusBuffer.size() > continouusBufferSize) {
            continouusBuffer.removeFirst();
//            Log.d("remove", "remove ");
        }
        continouusBuffer.add(value);

//        StringBuilder sb = new StringBuilder();
//        for (DTWValue dtwValue : continouusBuffer) {
//            sb.append(dtwValue + " ");
//        }
//
//        Log.d("buffer content", sb.toString());
    }



    public void detect() {
        // new value to buffer
        // run detection on complete buffer
        if (continouusBuffer.size() >= minTestValueCount) {
//            Log.d("buffer detect", "max: "+continouusBufferSize + " current: "+continouusBuffer.size());
            DTWValue[] testValues = new DTWValue[continouusBuffer.size()];

            for (int i = 0; i < continouusBuffer.size(); i++) {
                DTWValue dtwValue = continouusBuffer.get(i);
//                printDTWValues("copy "+i, new DTWValue[]{dtwValue});
                testValues[i] = dtwValue;
            }
//            printDTWValues("test values", testValues);
            runDTW(testValues);
        }


    }


    public void setRecognizedThreshold(double threshold) {
        Log.d("recognizer " + name + " " + id, "threshold: " + threshold);
        this.threshold = threshold;
    }

    public void clearBuffer() {
        continouusBuffer.clear();
    }

    private void runDTW(DTWValue[] testSeq) {
        // Log.d("gestures","------------running dtw");
        //  printArray("test", testSeq);
        DTWResult dtwResult = dtw.detect(testSeq);
        //     Log.d("gestures","Result: "+dtwResult);
        if (dtwResult.getDistance() <= threshold) {
            //  Log.d("gestures","++++++++++++++++++++++++++++Recognized+++++++++++++++++++++++");


            if (gestureRecognizedListener != null) {
                gestureRecognizedListener.onGestureRecognized(this, dtwResult);
            }
        }
    }


    public void setGestureRecognizedListener(GestureRecognizedListener gestureRecognizedListener) {
        this.gestureRecognizedListener = gestureRecognizedListener;
    }

    public interface GestureRecognizedListener {
        void onGestureRecognized(MultiGestureRecognizer sender, DTWResult result);
    }

    public static void printDTWValues(String tag, DTWValue[] values){
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < values.length; i++) {
            sb.append(" ");
            sb.append(values[i].toString());
        }

        Log.d("DTWValues", tag + ": "+sb.toString());
    }

}
