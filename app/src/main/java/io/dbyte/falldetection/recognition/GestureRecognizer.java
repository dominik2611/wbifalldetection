package io.dbyte.falldetection.recognition;

import android.util.Log;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class GestureRecognizer {


    private double[] reference;


    private RingBuffer<Double> buffer;

    private LinkedList<Double> continouusBuffer;
    private int continouusBufferSize;

    private DynamicTimeWarp dtw;

    private int maxTestSequenceLength;
    private int overlapSize; // values to overlap between sequences

    private int minTestValueCount; // minimal amount of test values to check gesture

    private double threshold;

    private GestureRecognizedListener gestureRecognizedListener;

    private String name;

    private int detectionIntervall = 20;

    private int id = 0;

    public GestureRecognizer(int id, String name, double[] reference, int minTestValueCount, double threshold, int bufferSize, int maxTestSequenceLength, int overlapSize) {
        this.reference = reference;
        printArray("reference for: "+id, reference);
        buffer = new RingBuffer<Double>(bufferSize, Double.class);
        dtw = new DynamicTimeWarp();
        dtw.setReferenceGesture(reference);
        this.maxTestSequenceLength = maxTestSequenceLength ;
        this.overlapSize = overlapSize;
        this.minTestValueCount = minTestValueCount;
        this.threshold = threshold;
        this.name = name;
this.id = id;
        continouusBuffer = new LinkedList<>();
        continouusBufferSize = reference.length *3;
       // continousBuffer = new RingBuffer<Float>(reference.length *2, Float.class);

    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public void addToBuffer(double value) {
        buffer.store(value);

        continouusBuffer.addLast(value);
        if(continouusBuffer.size() > continouusBufferSize){
            continouusBuffer.removeFirst();
        }


    }

    public void detect(){
        // new value to buffer
        // run detection on complete buffer
        if(continouusBuffer.size() >= minTestValueCount){
            double[] testValues = new double[continouusBuffer.size()];
            for (int i = 0; i < testValues.length; i++) {
                testValues[i] = continouusBuffer.get(i);
            }

            runDTW(testValues);
        }



    }

//    public void startRecognition() {
//        // get data from buffer
//        // read as long as maxLength not exceeded
//        ArrayList<Float> testSequence = new ArrayList<>();
//        int currentRead = 0;
//        Float testVal;
//
//        float[] overlapValues = new float[0];
//
//        do {
//            testVal = buffer.getNext();
//            if (testVal != null) {
//                testSequence.add(testVal);
//                currentRead++;
//            }
//
//            // stop reading if maxLength or no data available
//            if (currentRead >= maxTestSequenceLength || testVal == null) {
//                currentRead = 0;
//
//                // convert to array...
//                float[] floatData = new float[testSequence.size() + overlapValues.length];
//                int possibleOverlap = Math.min(testSequence.size(), overlapSize);
//                float[] tempOverlap = new float[possibleOverlap];
//                int overlapPos = 0;
//                for (int i = 0; i < floatData.length; i++) {
//
//                    // add former overlap
//                    if (i < overlapValues.length) {
//
//                        floatData[i] = overlapValues[i];
//                    } else {
//
//                        float d1 = testSequence.get(i - overlapValues.length);
//                        floatData[i] = d1;
//
//
//                        // fill temp overlap values
//                        if (i >= floatData.length - possibleOverlap && overlapPos < tempOverlap.length) {
//                            //     System.out.println("overlap : " + i + " possible: " + possibleOverlap);
//                            tempOverlap[overlapPos] = d1;
//                            overlapPos++;
//                        }
//                    }
//
//                }
//                //printArray("overlap: ",tempOverlap);
//                // do test
//                if(floatData.length >= minTestValueCount){
//                    runDTW(floatData);
//                }
//
//                testSequence = new ArrayList<>();
//                // store temp overlap into overlap
//                overlapValues = tempOverlap;
//                overlapPos = 0;
//
//
//            }
//
//        } while (testVal != null);
//    }

    public void setRecognizedThreshold(double threshold){
        Log.d("recognizer "+name + " "+id, "threshold: "+threshold);
        this.threshold = threshold;
    }

    public void clearBuffer(){
        continouusBuffer.clear();
    }

    private void runDTW(double[] testSeq) {
       // Log.d("gestures","------------running dtw");
      //  printArray("test", testSeq);
        DTWResult dtwResult = dtw.detect(testSeq);
      //  Log.d("gestures","Result: "+dtwResult);
        if(dtwResult.getDistance() <= threshold){
            //  Log.d("gestures","++++++++++++++++++++++++++++Recognized+++++++++++++++++++++++");


            if(gestureRecognizedListener != null){
                gestureRecognizedListener.onGestureRecognized(this, dtwResult);
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Reference set: ");
        for (double v : reference) {
            sb.append(v + " | ");
        }

        return "Recognizer " + sb.toString() + "\n" + buffer.toString();
    }

    private static void printArray(String name, double[] f) {
        Log.d("gestures","array: name: " + name);
        for (double v : f) {
            System.out.print(" " + v);
        }
        System.out.println();
    }

    public void setGestureRecognizedListener(GestureRecognizedListener gestureRecognizedListener) {
        this.gestureRecognizedListener = gestureRecognizedListener;
    }

    public interface GestureRecognizedListener {
        void onGestureRecognized(GestureRecognizer sender, DTWResult result);
    }

}
